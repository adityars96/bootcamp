package com.xsis.spring.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="jurusan")
public class Jurusan {
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE, generator="jurusan_seq")
	@TableGenerator(name="jurusan_seq", table="tbl_sequence", pkColumnName="seq_id",
	valueColumnName="seq_value", initialValue=0, allocationSize=1)
	
	@Column(name="id")
	public int id;
	
	@Column(name="kodejur", nullable=false)
	public String kodejur;
	
	@Column(name="namajur", nullable=false)
	public String namajur;
	
	@Column(name="kodefak", nullable=false)
	public String kodefak;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKodejur() {
		return kodejur;
	}

	public void setKodejur(String kodejur) {
		this.kodejur = kodejur;
	}

	public String getNamajur() {
		return namajur;
	}

	public void setNamajur(String namajur) {
		this.namajur = namajur;
	}

	public String getKodefak() {
		return kodefak;
	}

	public void setKodefak(String kodefak) {
		this.kodefak = kodefak;
	}
	
}
