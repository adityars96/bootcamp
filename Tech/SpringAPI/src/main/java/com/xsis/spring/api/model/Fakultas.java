package com.xsis.spring.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="fakultas")
public class Fakultas {

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE, generator="fakultas_seq")
	@TableGenerator(name="fakultas_seq", table="tbl_sequence", pkColumnName="seq_id",
	valueColumnName="seq_value", initialValue=0, allocationSize=1)
	
	@Column(name="id")
	public int id;
	
	@Column(name="kodefak", nullable=false, length=10)
	public String kodefak;
	
	@Column(name="namafak", nullable=false, length=100)
	public String namafak;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKodefak() {
		return kodefak;
	}

	public void setKodefak(String kodefak) {
		this.kodefak = kodefak;
	}

	public String getNamafak() {
		return namafak;
	}

	public void setNamafak(String namafak) {
		this.namafak = namafak;
	}
	
}
