package com.xsis.spring.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="biodata")
public class Biodata {
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE, generator="biodata_seq")
	@TableGenerator(name="biodata_seq", table="tbl_sequence", pkColumnName="seq_id",
	valueColumnName="seq_value", initialValue=0, allocationSize=1)
	
	@Column(name="id")
	public int id;
	
	@Column(name="nama", nullable=false)
	public String nama;
	
	@Column(name="alamat", nullable=false)
	public String alamat;
	
	@Column(name="jk", nullable=false)
	public String jk;
	
	@Column(name="telp", nullable=false)
	public String telp;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getJk() {
		return jk;
	}

	public void setJk(String jk) {
		this.jk = jk;
	}

	public String getTelp() {
		return telp;
	}

	public void setTelp(String telp) {
		this.telp = telp;
	}
	
}
