package com.xsis.spring.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.spring.api.model.Jurusan;

@Repository
public interface JurusanRepo extends JpaRepository<Jurusan, Integer> {

}
