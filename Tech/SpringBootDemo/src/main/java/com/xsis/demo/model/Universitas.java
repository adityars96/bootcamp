package com.xsis.demo.model;

public class Universitas {
	public String jur;
	public int ang;
	public String fak;
	public String univ;
	public String getJur() {
		return jur;
	}
	public void setJur(String jur) {
		this.jur = jur;
	}
	public int getAng() {
		return ang;
	}
	public void setAng(int ang) {
		this.ang = ang;
	}
	public String getFak() {
		return fak;
	}
	public void setFak(String fak) {
		this.fak = fak;
	}
	public String getUniv() {
		return univ;
	}
	public void setUniv(String univ) {
		this.univ = univ;
	}	
}
