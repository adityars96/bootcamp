package com.xsis.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Universitas;

@Controller
public class UnivController {
	@RequestMapping("/univ/add")
	public String add() {
		return "/univ/add";
	}
	
	@RequestMapping(value="/univ/save", method=RequestMethod.POST)
	public String save(@ModelAttribute Universitas item, Model model) {
		model.addAttribute("data", item);
		return "/univ/save";
	}
}
