package com.xsis.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Biodata;
import com.xsis.demo.repository.BiodataRepo;

@Controller
public class BiodataController {
	@Autowired
	private BiodataRepo repo;
	
	@RequestMapping("/biodata")
	public String index(Model model) {
		List<Biodata> data=repo.findAll();
		model.addAttribute("listData", data);
		return "biodata/index";
	}
	
	@RequestMapping("/biodata/add")
	public String add() {
		return "/biodata/add";
	}
	
	@RequestMapping(value="/biodata/save", method=RequestMethod.POST)
	public String save(@ModelAttribute Biodata item) {
		repo.save(item);
		return "redirect:/biodata";
	}
	
	@RequestMapping(value="/biodata/edit/{id}")
	public String edit(Model model, @PathVariable(name="id") Integer id) {
		Biodata item=repo.findById(id).orElse(null);
		model.addAttribute("data", item);
		return "biodata/edit";
	}
	
	@RequestMapping(value="/biodata/delete/{id}")
	public String delete(@PathVariable(name="id") Integer id) {
		Biodata item=repo.findById(id).orElse(null);
		if(item!=null) {
			repo.delete(item);
		}
		return "redirect:/biodata";
	}
}
