package com.xsis.demo.model;

public class Alamat {
	public String jl;
	public String kel;
	public String kec;
	public String kabkot;
	public String prov;
	public String getJl() {
		return jl;
	}
	public void setJl(String jl) {
		this.jl = jl;
	}
	public String getKel() {
		return kel;
	}
	public void setKel(String kel) {
		this.kel = kel;
	}
	public String getKec() {
		return kec;
	}
	public void setKec(String kec) {
		this.kec = kec;
	}
	public String getKabkot() {
		return kabkot;
	}
	public void setKabkot(String kabkot) {
		this.kabkot = kabkot;
	}
	public String getProv() {
		return prov;
	}
	public void setProv(String prov) {
		this.prov = prov;
	}
	
}
