package com.xsis.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Alamat;

@Controller
public class AlamatController {
	@RequestMapping("/alamat/add")
	public String add() {
		return "/alamat/add";
	}
	
	@RequestMapping(value="/alamat/save", method=RequestMethod.POST)
	public String save(@ModelAttribute Alamat item, Model model) {
		model.addAttribute("data", item);
		return "/alamat/save";
	}
}
