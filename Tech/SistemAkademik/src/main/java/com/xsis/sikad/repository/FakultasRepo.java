package com.xsis.sikad.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.sikad.model.FakultasModel;

@Repository
public interface FakultasRepo extends JpaRepository<FakultasModel, Long> {

}
