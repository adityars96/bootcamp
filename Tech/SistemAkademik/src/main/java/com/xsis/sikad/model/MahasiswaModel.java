package com.xsis.sikad.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;

@Entity
@Table(name="mahasiswa")
@Data
public class MahasiswaModel {
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE, generator="mahasiswa_seq")
	@TableGenerator(name="mahasiswa_seq", table="tbl_sequence", pkColumnName="seq_id",
	valueColumnName="seq_value", initialValue=0, allocationSize=1)
	@Column(name="id")
	private Long id;
	
	@Column(name="kd_mhs", nullable=false, length=10)
	private String kode;
	
	@Column(name="nama_mhs", nullable=false, length=100)
	private String nama;
	
	@Column(name="jk", nullable=false, length=10)
	private String jk;
	
	@Column(name="alamat", nullable=false, length=500)
	private String alamat;
	
	@Column(name="jurusan_id", nullable=false)
	private Long jurusanId;
	
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name="jurusan_id", foreignKey=@ForeignKey(name="fk_jurusan_mhs"), updatable=false, insertable=false)
	private JurusanModel jurusan;
	
	@Column(name="status", nullable=false, length=10)
	private String status;
	
}
