package com.xsis.sikad.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.sikad.model.MahasiswaModel;

@Repository
public interface MahasiswaRepo extends JpaRepository<MahasiswaModel, Long> {

}
