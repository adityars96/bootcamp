package com.xsis.sikad.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.sikad.model.JurusanModel;

@Repository
public interface JurusanRepo extends JpaRepository<JurusanModel, Long> {

}
