package com.xsis.sikad.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.sikad.model.MatkulModel;

@Repository
public interface MatkulRepo extends JpaRepository<MatkulModel, Long> {

}
