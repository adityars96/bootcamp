package com.xsis.sikad.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;

@Entity
@Table(name="fakultas")
@Data

public class FakultasModel {
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE, generator="fakultas_seq")
	@TableGenerator(name="fakultas_seq", table="tbl_sequence", pkColumnName="seq_id",
	valueColumnName="seq_value", initialValue=0, allocationSize=1)
	
	@Column(name="id")
	private Long Id;
	
	@Column(name="kd_fakultas", nullable=false, length=10)
	private String kode;
	
	@Column(name="nm_fakultas", nullable=false, length=150)
	private String nama;
	
	@JsonManagedReference
	@OneToMany(mappedBy="fakultas", cascade=CascadeType.ALL)
	private List<JurusanModel> listJurusan = new ArrayList<JurusanModel>();
	
	@Column(name="is_delete")
	private boolean isDelete;
}
