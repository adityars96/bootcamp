package hari01;

import java.util.Scanner;

public class Soal06 {
	static Scanner scn;
	
	public static void main(String[] args) {
		//1 5 * 13 17 * 25
		scn=new Scanner (System.in);
		System.out.print("Masukkan Nilai n : ");
		int n=scn.nextInt();
		int a=1;
		for (int i = 0; i < n; i++) {
			if ((i+1)%3==0) {
				System.out.print("*");
			} else {
				System.out.print(a);
			}
			a+=4;
			System.out.print(" ");
		}
	}
}
