package hari01;

public class Looping {
	
	//method main
	public static void main(String[] args) {
		int a=100;
		int b=50;
		int hasil1=tambah(a,b);
		int hasil2=kali(a,b);
		int hasil3=kurang(a,b);
		float hasil4=bagi(a,b);
		
		System.out.println("Hasil tambah = "+hasil1);
		System.out.println("Hasil kali = "+hasil2);
		System.out.println("Hasil kurang = "+hasil3);
		System.out.println("Hasil bagi = "+hasil4);
	}
	
	static int tambah(int x, int y) {
		return x+y;
	}
	
	static int kali(int m, int n) {
		return m*n;
	}
	
	static int kurang(int p, int q) {
		return p-q;
	}
	
	static float bagi(int a, int b) {
		return a/b;
	}
		
}
