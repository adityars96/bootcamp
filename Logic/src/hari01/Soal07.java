package hari01;

import java.util.Scanner;

public class Soal07 {
	static Scanner scn;
	
	public static void main(String[] args) {
		//2 4 8 16 32 64 128
		scn=new Scanner (System.in);
		System.out.print("Masukkan Nilai n : ");
		int n=scn.nextInt();
		int a=2;
		for (int i = 0; i < n; i++) {
			System.out.print(a);
			a*=2;
			System.out.print(" ");
		}
	}

}
