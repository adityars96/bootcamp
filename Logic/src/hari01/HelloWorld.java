package hari01;

public class HelloWorld {

	public static void main(String[] args) {
		System.out.println("Welcome to Java");
		System.out.println("");
		makanFavorit();
		sampleObject();
	}	
	
	public static void sampleObject() {
		
		Orang or01 = new Orang();
		or01.nama="Via";
		or01.alamat="Magelang";
		or01.jk="Wanita";
		or01.tptlahir="Lampung";
		or01.umur=22;
		or01.cetak();
		
		Orang or02 = new Orang();
		or02.nama="Siti";
		or02.alamat="Yogyakarta";
		or02.jk="Wanita";
		or02.tptlahir="Solo";
		or02.umur=23;
		or02.cetak();
		
		Orang or03 = new Orang();
		or03.nama="Yayan";
		or03.alamat="Bandung";
		or03.jk="Pria";
		or03.tptlahir="Garut";
		or03.umur=20;
		or03.cetak();
		
		Orang or04 = new Orang();
		or04.nama="Jono";
		or04.alamat="Jakarta";
		or04.jk="Pria";
		or04.tptlahir="Kebumen";
		or04.umur=25;
		or04.cetak();
		
		Orang or05 = new Orang();
		or05.nama="Santi";
		or05.alamat="Surabaya";
		or05.jk="Wanita";
		or05.tptlahir="Semarang";
		or05.umur=23;
		or05.cetak();
		
	}
	
	public static void makanFavorit() {
		System.out.println("1. Rendang");
		System.out.println("2. Sate");
		System.out.println("3. Opor");
		System.out.println("4. Bakso");
		System.out.println("5. Soto");
		System.out.println("");
	}

}
