package hari01;

import java.util.Scanner;

public class Soal01 {
	static Scanner scn;
	
	public static void main(String[] args) {
		//1 3 5 7 9 11 13
		scn=new Scanner(System.in);
		System.out.print("Masukkan Nilai n : ");
		int n=scn.nextInt();
		int a=1;
		for (int i = 0; i < n; i++) {
			System.out.print(a);
			System.out.print(" ");
			a+=2;
		}
	}
}