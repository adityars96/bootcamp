package hari01;

import java.util.Scanner;

public class Soal09 {
	static Scanner scn;
	
	public static void main(String[] args) {
		//4 16 * 64 256 * 1024
		scn=new Scanner (System.in);
		System.out.print("Masukkan Nilai n : ");
		int n=scn.nextInt();
		int a=4;
		for (int i = 0; i < n; i++) {
			if ((i+1)%3==0) {
				System.out.print("*");
			} else {
				System.out.print(a);
				a*=4;
			}
			System.out.print(" ");
		}
	}
}
