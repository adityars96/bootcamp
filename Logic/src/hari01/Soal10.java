package hari01;

import java.util.Scanner;

public class Soal10 {
	static Scanner scn;
	
	public static void main(String[] args) {
		//3 9 27 xxx 243 729 2187
		scn=new Scanner (System.in);
		System.out.print("Masukkan Nilai n : ");
		int n=scn.nextInt();
		int a=3;
		for (int i = 0; i < n; i++) {
			if ((i+1)%4==0) {
				System.out.print("XXX");
			} else {
				System.out.print(a);
			}
			a*=3;
			System.out.print(" ");
		}
	}
}
