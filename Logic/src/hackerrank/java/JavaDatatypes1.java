package hackerrank.java;

import java.util.Scanner;

public class JavaDatatypes1 {
	public static void main(String []argh) {
        Scanner scan1=new Scanner(System.in);
        System.out.print("Masukkan jumlah tes : ");
        int n1=scan1.nextInt();
        for (int i = 0; i < n1; i++) {
            try {
            	System.out.print("Masukkan angka : ");
                long x1=scan1.nextLong();
                System.out.println(x1+" can be fitted in:");
                if(x1>=-128 && x1<=127){
                    System.out.println("* byte");
                }
                if(x1>=-1*Math.pow(2,15) && x1<=Math.pow(2,15)-1){
                    System.out.println("* short");
                }
                if(x1>=-1*Math.pow(2,31) && x1<=Math.pow(2,31)-1){
                    System.out.println("* int");
                }
                if(x1>=-1*Math.pow(2,63) && x1<=Math.pow(2,63)-1){
                    System.out.println("* long");
                }
            }
            catch(Exception e) {
                System.out.println(scan1.next()+" can't be fitted anywhere.");
            }
        }
    }
}
