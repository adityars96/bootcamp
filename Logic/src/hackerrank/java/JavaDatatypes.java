package hackerrank.java;

import java.util.*;

public class JavaDatatypes {
	public static void main(String []argh) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukkan jumlah tes : ");
        int n=scan.nextInt(); //input jumlah tes

        for(int i=0;i<n;i++){
            try {
            	System.out.print("Masukkan angka : ");
                long x=scan.nextLong(); //input angka
                System.out.println(x+" can be fitted in:");
                if(x>=-128 && x<=127){ //kategori byte
                    System.out.println("* byte");
                }
                if(x>=-1*Math.pow(2,15) && x<=Math.pow(2,15)-1){ //kategori short
                    System.out.println("* short");
                }
                if(x>=-1*Math.pow(2,31) && x<=Math.pow(2,31)-1){ //kategori int
                    System.out.println("* int");
                }
                if(x>=-1*Math.pow(2,63) && x<=Math.pow(2,63)-1){ //kategori long
                    System.out.println("* long");
                }
                
            }
            catch(Exception e) {
                System.out.println(scan.next()+" can't be fitted anywhere."); //yang lainnya
            }

        }
    }
}
