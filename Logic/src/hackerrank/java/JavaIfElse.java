package hackerrank.java;

import java.util.*;

public class JavaIfElse {
    public static void main(String[] args) {
    	Scanner scn=new Scanner(System.in);
    	System.out.print("Masukkan angka : ");
        int N = scn.nextInt(); //input angka
        if(N%2==1){
            System.out.println("Weird"); //jika ganjil maka weird
        } else{
            if(N>=2 && N<=5){ //genap antara 2-5 not weird
                System.out.println("Not Weird");
            }else if(N>=6 && N<=20){ //genap antara 6-20 weird
                System.out.println("Weird");
            }else { //genap >20 not weird
                System.out.println("Not Weird");
            }
        }
    }
}
