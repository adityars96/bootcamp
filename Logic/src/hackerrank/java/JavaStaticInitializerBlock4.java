package hackerrank.java;

import java.util.Scanner;

public class JavaStaticInitializerBlock4 {
	static int b4;
	static int h4;
	static Boolean flag4=false;
	static {
		Scanner scn4=new Scanner(System.in);
		System.out.print("Masukkan breadth : ");
		b4=scn4.nextInt();
		System.out.print("Masukkan height  : ");
		h4=scn4.nextInt();
		if(b4>0 && h4>0) {
			flag4=true;
		} else {
			System.out.print("java.lang.Exception: Breadth and height must be positive");
		}
	}
	public static void main(String[] args) {
		if(flag4) {
			int area4=b4*h4;
			System.out.print("Area : "+area4);
		}
	}
}
