package hackerrank.java;

import java.util.Scanner;

public class JavaStdinAndStdoutII1 {
	public static void main(String[] args) {
        Scanner scan1 = new Scanner(System.in);
        System.out.print("Masukkan integer : ");
        int i1 = scan1.nextInt();
        System.out.print("Masukkan double : ");
        double d1 = scan1.nextDouble();
        scan1.nextLine();
        System.out.print("Masukkan string : ");
        String s1 = scan1.nextLine();
        System.out.println();
        System.out.println("String : " + s1);
        System.out.println("Double : " + d1);
        System.out.println("Int    : " + i1);
    }
}
