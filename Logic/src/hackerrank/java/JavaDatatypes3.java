package hackerrank.java;

import java.util.Scanner;

public class JavaDatatypes3 {
	public static void main(String []argh) {
        Scanner scan3=new Scanner(System.in);
        System.out.print("Masukkan jumlah tes : ");
        int n3=scan3.nextInt();
        for (int i = 0; i < n3; i++) {
            try {
            	System.out.print("Masukkan angka : ");
                long x3=scan3.nextLong();
                System.out.println(x3+" can be fitted in:");
                if(x3>=-128 && x3<=127){
                    System.out.println("* byte");
                }
                if(x3>=-1*Math.pow(2,15) && x3<=Math.pow(2,15)-1){
                    System.out.println("* short");
                }
                if(x3>=-1*Math.pow(2,31) && x3<=Math.pow(2,31)-1){
                    System.out.println("* int");
                }
                if(x3>=-1*Math.pow(2,63) && x3<=Math.pow(2,63)-1){
                    System.out.println("* long");
                }
            }
            catch(Exception e) {
                System.out.println(scan3.next()+" can't be fitted anywhere.");
            }
        }
    }
}
