package hackerrank.java;

import java.util.Scanner;

public class JavaLoopsI3 {
	public static void main(String[] args) {
		Scanner scan3=new Scanner(System.in);
		System.out.print("Masukkan angka : ");
		int N3=scan3.nextInt();
		int kali3=0;
		for (int i = 1; i <= 10; i++) {
			kali3=N3*i;
			System.out.println(N3+" x "+i+" = "+kali3);
		}
	}
}
