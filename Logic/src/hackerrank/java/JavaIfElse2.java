package hackerrank.java;

import java.util.Scanner;

public class JavaIfElse2 {

	public static void main(String[] args) {
    	Scanner scn2=new Scanner(System.in);
    	System.out.print("Masukkan angka : ");
        int N2=scn2.nextInt();
        if(N2%2==1){
            System.out.println("Weird");
        } else{
            if(N2>=2 && N2<=5){
                System.out.println("Not Weird");
            }else if(N2>=6 && N2<=20){
                System.out.println("Weird");
            }else {
                System.out.println("Not Weird");
            }
        }
    }
}
