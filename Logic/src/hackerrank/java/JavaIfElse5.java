package hackerrank.java;

import java.util.Scanner;

public class JavaIfElse5 {

	public static void main(String[] args) {
    	Scanner scn5=new Scanner(System.in);
    	System.out.print("Masukkan angka : ");
        int N5=scn5.nextInt();
        if(N5%2==1){
            System.out.println("Weird");
        } else{
            if(N5>=2 && N5<=5){
                System.out.println("Not Weird");
            }else if(N5>=6 && N5<=20){
                System.out.println("Weird");
            }else {
                System.out.println("Not Weird");
            }
        }
    }
}
