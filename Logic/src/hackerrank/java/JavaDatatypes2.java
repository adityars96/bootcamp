package hackerrank.java;

import java.util.Scanner;

public class JavaDatatypes2 {
	public static void main(String []argh) {
        Scanner scan2=new Scanner(System.in);
        System.out.print("Masukkan jumlah tes : ");
        int n2=scan2.nextInt();
        for (int i = 0; i < n2; i++) {
            try {
            	System.out.print("Masukkan angka : ");
                long x2=scan2.nextLong();
                System.out.println(x2+" can be fitted in:");
                if(x2>=-128 && x2<=127){
                    System.out.println("* byte");
                }
                if(x2>=-1*Math.pow(2,15) && x2<=Math.pow(2,15)-1){
                    System.out.println("* short");
                }
                if(x2>=-1*Math.pow(2,31) && x2<=Math.pow(2,31)-1){
                    System.out.println("* int");
                }
                if(x2>=-1*Math.pow(2,63) && x2<=Math.pow(2,63)-1){
                    System.out.println("* long");
                }
            }
            catch(Exception e) {
                System.out.println(scan2.next()+" can't be fitted anywhere.");
            }
        }
    }
}
