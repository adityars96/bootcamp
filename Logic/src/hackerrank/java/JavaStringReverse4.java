package hackerrank.java;

import java.util.Scanner;

public class JavaStringReverse4 {
	public static void main(String[] args) {
        Scanner sc4=new Scanner(System.in);
        System.out.print("Masukkan text : ");
        String kata4=sc4.next();
        System.out.println("Apakah '"+kata4+"' adalah sebuah palindrom?");
        String result4=kata4.equals(new StringBuilder(kata4).reverse().toString())?"Yes":"No";
        System.out.print(result4);
    }
}
