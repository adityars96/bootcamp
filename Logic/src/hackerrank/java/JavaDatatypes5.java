package hackerrank.java;

import java.util.Scanner;

public class JavaDatatypes5 {

	public static void main(String []argh) {
        Scanner scan5=new Scanner(System.in);
        System.out.print("Masukkan jumlah tes : ");
        int n5=scan5.nextInt();
        for (int i = 0; i < n5; i++) {
            try {
            	System.out.print("Masukkan angka : ");
                long x5=scan5.nextLong();
                System.out.println(x5+" can be fitted in:");
                if(x5>=-128 && x5<=127){
                    System.out.println("* byte");
                }
                if(x5>=-1*Math.pow(2,15) && x5<=Math.pow(2,15)-1){
                    System.out.println("* short");
                }
                if(x5>=-1*Math.pow(2,31) && x5<=Math.pow(2,31)-1){
                    System.out.println("* int");
                }
                if(x5>=-1*Math.pow(2,63) && x5<=Math.pow(2,63)-1){
                    System.out.println("* long");
                }
            }
            catch(Exception e) {
                System.out.println(scan5.next()+" can't be fitted anywhere.");
            }
        }
    }
}
