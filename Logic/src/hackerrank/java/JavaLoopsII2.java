package hackerrank.java;

import java.util.Scanner;

public class JavaLoopsII2 {
	public static void main(String []argh){
		System.out.println("Akan dibuat deret dengan pola S(n)=S(n-1)+b*2^n dimana S(0)=a+b*2^0");
        Scanner scan2=new Scanner(System.in);
        System.out.print("Masukkan jumlah tes : ");
        int q2=scan2.nextInt();
        for(int i=0;i<q2;i++){
        	System.out.print("Masukkan a : ");
            int a2 = scan2.nextInt();
            System.out.print("Masukkan b : ");
            int b2 = scan2.nextInt();
            System.out.print("Masukkan n : ");
            int n2 = scan2.nextInt();

            for(int j=0;j<n2;j++){
                a2+=b2;
                System.out.print(a2+" ");
                b2*=2;
            }
            System.out.println();
        }
    }
}
