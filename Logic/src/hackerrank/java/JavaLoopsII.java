package hackerrank.java;

import java.util.*;

public class JavaLoopsII {
	public static void main(String []argh){
		System.out.println("Akan dibuat deret dengan pola S(n)=S(n-1)+b*2^n dimana S(0)=a+b*2^0");
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukkan jumlah tes : ");
        int q=scan.nextInt(); //input jumlah tes
        for(int i=0;i<q;i++){
        	System.out.print("Masukkan a : ");
            int a = scan.nextInt(); //input a
            System.out.print("Masukkan b : ");
            int b = scan.nextInt(); //input b
            System.out.print("Masukkan n : ");
            int n = scan.nextInt(); //input panjang deret

            for(int j=0;j<n;j++){
                a=a+b; //update a
                System.out.print(a+" ");
                b=b*2; //update b
            }
            System.out.println();
        }
    }
}
