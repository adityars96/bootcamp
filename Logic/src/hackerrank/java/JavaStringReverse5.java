package hackerrank.java;

import java.util.Scanner;

public class JavaStringReverse5 {

	public static void main(String[] args) {
        Scanner sc5=new Scanner(System.in);
        System.out.print("Masukkan text : ");
        String kata5=sc5.next();
        System.out.println("Apakah '"+kata5+"' adalah sebuah palindrom?");
        String result5=kata5.equals(new StringBuilder(kata5).reverse().toString())?"Yes":"No";
        System.out.print(result5);
    }
}
