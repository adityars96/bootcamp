package hackerrank.java;

import java.util.Scanner;

public class JavaDatatypes4 {
	public static void main(String []argh) {
        Scanner scan4=new Scanner(System.in);
        System.out.print("Masukkan jumlah tes : ");
        int n4=scan4.nextInt();
        for (int i = 0; i < n4; i++) {
            try {
            	System.out.print("Masukkan angka : ");
                long x4=scan4.nextLong();
                System.out.println(x4+" can be fitted in:");
                if(x4>=-128 && x4<=127){
                    System.out.println("* byte");
                }
                if(x4>=-1*Math.pow(2,15) && x4<=Math.pow(2,15)-1){
                    System.out.println("* short");
                }
                if(x4>=-1*Math.pow(2,31) && x4<=Math.pow(2,31)-1){
                    System.out.println("* int");
                }
                if(x4>=-1*Math.pow(2,63) && x4<=Math.pow(2,63)-1){
                    System.out.println("* long");
                }
            }
            catch(Exception e) {
                System.out.println(scan4.next()+" can't be fitted anywhere.");
            }
        }
    }
}
