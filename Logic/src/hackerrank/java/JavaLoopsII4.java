package hackerrank.java;

import java.util.Scanner;

public class JavaLoopsII4 {
	public static void main(String []argh){
		System.out.println("Akan dibuat deret dengan pola S(n)=S(n-1)+b*2^n dimana S(0)=a+b*2^0");
        Scanner scan4=new Scanner(System.in);
        System.out.print("Masukkan jumlah tes : ");
        int q4=scan4.nextInt();
        for(int i=0;i<q4;i++){
        	System.out.print("Masukkan a : ");
            int a4=scan4.nextInt();
            System.out.print("Masukkan b : ");
            int b4=scan4.nextInt();
            System.out.print("Masukkan n : ");
            int n4=scan4.nextInt();

            for(int j=0;j<n4;j++){
                a4+=b4;
                System.out.print(a4+" ");
                b4*=2;
            }
            System.out.println();
        }
    }
}
