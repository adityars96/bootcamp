package hackerrank.java;

import java.util.Scanner;

public class JavaStaticInitializerBlock3 {
	static int b3;
	static int h3;
	static Boolean flag3=false;
	static {
		Scanner scn3=new Scanner(System.in);
		System.out.print("Masukkan breadth : ");
		b3=scn3.nextInt();
		System.out.print("Masukkan height  : ");
		h3=scn3.nextInt();
		if(b3>0 && h3>0) {
			flag3=true;
		} else {
			System.out.print("java.lang.Exception: Breadth and height must be positive");
		}
	}
	public static void main(String[] args) {
		if(flag3) {
			int area3=b3*h3;
			System.out.print("Area : "+area3);
		}
	}
}
