package hackerrank.java;

import java.util.Scanner;

public class JavaStaticInitializerBlock {
	static int b;
	static int h;
	static Boolean flag=false;
	static {
		Scanner scn=new Scanner(System.in);
		System.out.print("Masukkan Breadth : ");
		b=scn.nextInt(); //input breadth
		System.out.print("Masukkan Height  : ");
		h=scn.nextInt(); //input height
		if(b>0 && h>0) {
			flag=true;
		} else {
			System.out.print("java.lang.Exception: Breadth and height must be positive");
		}
	}
	
	public static void main(String[] args) {
		if(flag) { //jika true maka ...
			int area=b*h;
			System.out.print("Area : "+area);
		}
	}
}
