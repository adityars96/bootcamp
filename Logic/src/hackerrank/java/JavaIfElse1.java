package hackerrank.java;

import java.util.*;

public class JavaIfElse1 {
	public static void main(String[] args) {
    	Scanner scn1=new Scanner(System.in);
    	System.out.print("Masukkan angka : ");
        int N1 = scn1.nextInt();
        if(N1%2==1){
            System.out.println("Weird");
        } else{
            if(N1>=2 && N1<=5){
                System.out.println("Not Weird");
            }else if(N1>=6 && N1<=20){
                System.out.println("Weird");
            }else {
                System.out.println("Not Weird");
            }
        }
    }
}
