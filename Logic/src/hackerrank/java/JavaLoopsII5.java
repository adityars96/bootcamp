package hackerrank.java;

import java.util.Scanner;

public class JavaLoopsII5 {
	public static void main(String []argh){
		System.out.println("Akan dibuat deret dengan pola S(n)=S(n-1)+b*2^n dimana S(0)=a+b*2^0");
        Scanner scan5=new Scanner(System.in);
        System.out.print("Masukkan jumlah tes : ");
        int q5=scan5.nextInt();
        for(int i=0;i<q5;i++){
        	System.out.print("Masukkan a : ");
            int a5=scan5.nextInt();
            System.out.print("Masukkan b : ");
            int b5=scan5.nextInt();
            System.out.print("Masukkan n : ");
            int n5=scan5.nextInt();

            for(int j=0;j<n5;j++){
                a5+=b5;
                System.out.print(a5+" ");
                b5*=2;
            }
            System.out.println();
        }
    }
}
