package hackerrank.java;

import java.util.Scanner;

public class JavaLoopsI4 {
	public static void main(String[] args) {
		Scanner scan4=new Scanner(System.in);
		System.out.print("Masukkan angka : ");
		int N4=scan4.nextInt();
		int kali4=0;
		for (int i = 1; i <= 10; i++) {
			kali4=N4*i;
			System.out.println(N4+" x "+i+" = "+kali4);
		}
	}
}
