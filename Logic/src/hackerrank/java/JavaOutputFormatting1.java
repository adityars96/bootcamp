package hackerrank.java;

import java.util.Scanner;

public class JavaOutputFormatting1 {
	public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        String[] kata1=new String[3];
        int[] angka1=new int[3];
        for (int i = 0; i < 3; i++) {
        	System.out.print("Masukkan text : ");
        	kata1[i]=sc1.next();
        	System.out.print("Masukkan angka 0-999 : ");
            angka1[i]=sc1.nextInt();
		}
        System.out.println("================================");
        for(int i = 0; i < 3 ; i++) {
            System.out.printf("%-15s%s %n",kata1[i],String.format("%03d",angka1[i]));
        }
        System.out.println("================================");
	}
}
