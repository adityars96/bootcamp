package hackerrank.java;


import java.util.*;

public class JavaLoopsI {

    public static void main(String[] args) {
    	Scanner scan = new Scanner(System.in);
    	System.out.print("Masukkan angka : ");
        int N = scan.nextInt(); //input angka
        int kali=0;
        for (int i = 1; i <= 10; i++) {
            kali=N*i; //perkalian N*i
            System.out.println(N+" x "+i+" = "+kali);
        }
    }
}
