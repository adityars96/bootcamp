package hackerrank.java;

import java.util.Scanner;

public class JavaStaticInitializerBlock2 {
	static int b2;
	static int h2;
	static Boolean flag2=false;
	static {
		Scanner scn2=new Scanner(System.in);
		System.out.print("Masukkan breadth : ");
		b2=scn2.nextInt();
		System.out.print("Masukkan height  : ");
		h2=scn2.nextInt();
		if(b2>0 && h2>0) {
			flag2=true;
		} else {
			System.out.print("java.lang.Exception: Breadth and height must be positive");
		}
	}
	public static void main(String[] args) {
		if(flag2) {
			int area2=b2*h2;
			System.out.print("Area : "+area2);
		}
	}
}
