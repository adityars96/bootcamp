package hackerrank.java;

import java.util.Scanner;

public class JavaStdinAndStdoutII2 {
	public static void main(String[] args) {
        Scanner scan2 = new Scanner(System.in);
        System.out.print("Masukkan integer : ");
        int i2 = scan2.nextInt();
        System.out.print("Masukkan double : ");
        double d2 = scan2.nextDouble();
        scan2.nextLine();
        System.out.print("Masukkan string : ");
        String s2 = scan2.nextLine();
        System.out.println();
        System.out.println("String : " + s2);
        System.out.println("Double : " + d2);
        System.out.println("Int    : " + i2);
    }
}
