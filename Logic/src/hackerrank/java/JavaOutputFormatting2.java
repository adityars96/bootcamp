package hackerrank.java;

import java.util.Scanner;

public class JavaOutputFormatting2 {

	public static void main(String[] args) {
        Scanner sc2=new Scanner(System.in);
        String[] kata2=new String[3];
        int[] angka2=new int[3];
        for (int i = 0; i < 3; i++) {
        	System.out.print("Masukkan text : ");
        	kata2[i]=sc2.next();
        	System.out.print("Masukkan angka 0-999 : ");
            angka2[i]=sc2.nextInt();
		}
        System.out.println("================================");
        for(int i = 0; i < 3 ; i++) {
            System.out.printf("%-15s%s %n",kata2[i],String.format("%03d",angka2[i]));
        }
        System.out.println("================================");
	}
}
