package hackerrank.java;

import java.util.Scanner;

public class JavaLoopsII1 {
	public static void main(String []argh){
		System.out.println("Akan dibuat deret dengan pola S(n)=S(n-1)+b*2^n dimana S(0)=a+b*2^0");
        Scanner scan1=new Scanner(System.in);
        System.out.print("Masukkan jumlah tes : ");
        int q1=scan1.nextInt();
        for(int i=0;i<q1;i++){
        	System.out.print("Masukkan a : ");
            int a1=scan1.nextInt();
            System.out.print("Masukkan b : ");
            int b1=scan1.nextInt();
            System.out.print("Masukkan n : ");
            int n1=scan1.nextInt();

            for(int j=0;j<n1;j++){
                a1+=b1;
                System.out.print(a1+" ");
                b1*=2;
            }
            System.out.println();
        }
    }
}
