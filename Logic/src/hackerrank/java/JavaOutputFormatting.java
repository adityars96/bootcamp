package hackerrank.java;

import java.util.Scanner;

public class JavaOutputFormatting {
	public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String[] kata=new String[3]; //membuat array string[3]
        int[] angka=new int[3]; //membuat array int[3]
        for (int i = 0; i < 3; i++) {
        	System.out.print("Masukkan text : ");
        	kata[i]=sc.next();
        	System.out.print("Masukkan angka 0-999 : ");
            angka[i]=sc.nextInt();
		}
        System.out.println("================================");
        for(int i=0;i<3;i++)
        {
            System.out.printf("%-15s%s %n",kata[i],String.format("%03d",angka[i])); //print format string 15 char rata kiri & int 3 digit 
        }
        System.out.println("================================");
	}
}
