package hackerrank.java;

import java.util.Scanner;

public class JavaStdinAndStdoutII5 {
	public static void main(String[] args) {
        Scanner scan5 = new Scanner(System.in);
        System.out.print("Masukkan integer : ");
        int i5 = scan5.nextInt();
        System.out.print("Masukkan double : ");
        double d5 = scan5.nextDouble();
        scan5.nextLine();
        System.out.print("Masukkan string : ");
        String s5 = scan5.nextLine();
        System.out.println();
        System.out.println("String : " + s5);
        System.out.println("Double : " + d5);
        System.out.println("Int    : " + i5);
    }
}
