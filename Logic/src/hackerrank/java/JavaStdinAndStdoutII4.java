package hackerrank.java;

import java.util.Scanner;

public class JavaStdinAndStdoutII4 {
	public static void main(String[] args) {
        Scanner scan4 = new Scanner(System.in);
        System.out.print("Masukkan integer : ");
        int i4 = scan4.nextInt();
        System.out.print("Masukkan double : ");
        double d4 = scan4.nextDouble();
        scan4.nextLine();
        System.out.print("Masukkan string : ");
        String s4 = scan4.nextLine();
        System.out.println();
        System.out.println("String : " + s4);
        System.out.println("Double : " + d4);
        System.out.println("Int    : " + i4);
    }
}
