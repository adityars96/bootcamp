package hackerrank.java;

import java.util.*;

public class Anagram {
	public static void main(String[] args) {
		System.out.println(isAnagram("Hello", "Lolhe"));
	}
	public static boolean isAnagram(String a, String b) {
		Map<String, Integer> map=new HashMap();
		String[] a1=a.toLowerCase().split("");
		String[] b1=b.toLowerCase().split("");
		for (int i = 0; i < a1.length; i++) {
			if(map.containsKey(a1[i])) {
				int n=map.get(a1[i]);
				n++;
				map.put(a1[i], n);
			} else {
				map.put(a1[i], 1);
			}
		}
		for (int j = 0; j < b1.length; j++) {
			if(!map.containsKey(b1[j])) {
				return false;
			}
			int n=map.get(b1[j]);
			if(n==0) {
				return false;
			} else {
				n--;
				map.put(b1[j], n);
			}
		} return true;
	}
}