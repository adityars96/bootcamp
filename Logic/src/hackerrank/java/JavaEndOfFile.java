package hackerrank.java;

import java.util.*;

public class JavaEndOfFile {
	public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukkan N : ");
        int n=scan.nextInt(); //input n
        Scanner scn = new Scanner(System.in);
        String[] line=new String[n]; //membuat array berisi text sebanyak n
        for(int i=0; i<n ;i++){
            System.out.print("Masukkan text : ");
            line[i]=scn.nextLine(); //input text ke-i
        }
        for (int j = 1; j <= n; j++) {
        	System.out.println(j+" "+line[j-1]); //print text
		}
    }
}
