package hackerrank.java;

import java.util.Scanner;

public class JavaStdinAndStdoutI1 {
	public static void main(String[] args) {
        Scanner scan1 = new Scanner(System.in);
        System.out.print("Masukkan integer 1 : ");
        int a1= scan1.nextInt();
        System.out.print("Masukkan integer 2 : ");
        int b1= scan1.nextInt();
        System.out.print("Masukkan integer 3 : ");
        int c1=scan1.nextInt();
        System.out.println();
        System.out.println(a1);
        System.out.println(b1);
        System.out.println(c1);
    }
}
