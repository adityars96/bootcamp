package hackerrank.java;

import java.util.Scanner;

public class JavaLoopsI1 {
	public static void main(String[] args) {
		Scanner scan1=new Scanner(System.in);
		System.out.print("Masukkan angka : ");
		int N1=scan1.nextInt();
		int kali1=0;
		for (int i = 1; i <= 10; i++) {
			kali1=N1*i;
			System.out.println(N1+" x "+i+" = "+kali1);
		}
	}
}
