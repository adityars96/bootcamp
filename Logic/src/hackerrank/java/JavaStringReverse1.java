package hackerrank.java;

import java.util.Scanner;

public class JavaStringReverse1 {
	public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.print("Masukkan text : ");
        String kata1=sc1.next();
        System.out.println("Apakah '"+kata1+"' adalah sebuah palindrom?");
        String result1=kata1.equals(new StringBuilder(kata1).reverse().toString())?"Yes":"No";
        System.out.print(result1);
    }
}
