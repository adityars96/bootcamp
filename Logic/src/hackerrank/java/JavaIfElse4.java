package hackerrank.java;

import java.util.Scanner;

public class JavaIfElse4 {
	public static void main(String[] args) {
    	Scanner scn4=new Scanner(System.in);
    	System.out.print("Masukkan angka : ");
        int N4=scn4.nextInt();
        if(N4%2==1){
            System.out.println("Weird");
        } else{
            if(N4>=2 && N4<=5){
                System.out.println("Not Weird");
            }else if(N4>=6 && N4<=20){
                System.out.println("Weird");
            }else {
                System.out.println("Not Weird");
            }
        }
    }
}
