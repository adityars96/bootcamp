package hackerrank.java;

import java.util.Scanner;

public class JavaStaticInitializerBlock5 {
	static int b5;
	static int h5;
	static Boolean flag5=false;
	static {
		Scanner scn5=new Scanner(System.in);
		System.out.print("Masukkan breadth : ");
		b5=scn5.nextInt();
		System.out.print("Masukkan height  : ");
		h5=scn5.nextInt();
		if(b5>0 && h5>0) {
			flag5=true;
		} else {
			System.out.print("java.lang.Exception: Breadth and height must be positive");
		}
	}
	public static void main(String[] args) {
		if(flag5) {
			int area5=b5*h5;
			System.out.print("Area : "+area5);
		}
	}
}
