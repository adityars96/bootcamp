package hackerrank.java;

import java.util.Scanner;

public class JavaStaticInitializerBlock1 {
	static int b1;
	static int h1;
	static Boolean flag1=false;
	static {
		Scanner scn1=new Scanner(System.in);
		System.out.print("Masukkan breadth : ");
		b1=scn1.nextInt();
		System.out.print("Masukkan height  : ");
		h1=scn1.nextInt();
		if(b1>0 && h1>0) {
			flag1=true;
		} else {
			System.out.print("java.lang.Exception: Breadth and height must be positive");
		}
	}
	public static void main(String[] args) {
		if(flag1) {
			int area1=b1*h1;
			System.out.print("Area : "+area1);
		}
	}
}
