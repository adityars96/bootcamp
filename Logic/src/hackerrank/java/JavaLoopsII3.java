package hackerrank.java;

import java.util.Scanner;

public class JavaLoopsII3 {

	public static void main(String []argh){
		System.out.println("Akan dibuat deret dengan pola S(n)=S(n-1)+b*2^n dimana S(0)=a+b*2^0");
        Scanner scan3=new Scanner(System.in);
        System.out.print("Masukkan jumlah tes : ");
        int q3=scan3.nextInt();
        for(int i=0;i<q3;i++){
        	System.out.print("Masukkan a : ");
            int a3=scan3.nextInt();
            System.out.print("Masukkan b : ");
            int b3=scan3.nextInt();
            System.out.print("Masukkan n : ");
            int n3=scan3.nextInt();

            for(int j=0;j<n3;j++){
                a3+=b3;
                System.out.print(a3+" ");
                b3*=2;
            }
            System.out.println();
        }
    }
}
