package hackerrank.java;

import java.util.Scanner;

public class JavaOutputFormatting5 {
	public static void main(String[] args) {
        Scanner sc5=new Scanner(System.in);
        String[] kata5=new String[3];
        int[] angka5=new int[3];
        for (int i = 0; i < 3; i++) {
        	System.out.print("Masukkan text : ");
        	kata5[i]=sc5.next();
        	System.out.print("Masukkan angka 0-999 : ");
            angka5[i]=sc5.nextInt();
		}
        System.out.println("================================");
        for(int i = 0; i < 3 ; i++) {
            System.out.printf("%-15s%s %n",kata5[i],String.format("%03d",angka5[i]));
        }
        System.out.println("================================");
	}
}
