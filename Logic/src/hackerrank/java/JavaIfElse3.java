package hackerrank.java;

import java.util.Scanner;

public class JavaIfElse3 {
	public static void main(String[] args) {
    	Scanner scn3=new Scanner(System.in);
    	System.out.print("Masukkan angka : ");
        int N3=scn3.nextInt();
        if(N3%2==1){
            System.out.println("Weird");
        } else{
            if(N3>=2 && N3<=5){
                System.out.println("Not Weird");
            }else if(N3>=6 && N3<=20){
                System.out.println("Weird");
            }else {
                System.out.println("Not Weird");
            }
        }
    }
}
