package hackerrank.java;

import java.util.Scanner;

public class JavaStdinAndStdoutII3 {
	public static void main(String[] args) {
        Scanner scan3 = new Scanner(System.in);
        System.out.print("Masukkan integer : ");
        int i3 = scan3.nextInt();
        System.out.print("Masukkan double : ");
        double d3 = scan3.nextDouble();
        scan3.nextLine();
        System.out.print("Masukkan string : ");
        String s3 = scan3.nextLine();
        System.out.println();
        System.out.println("String : " + s3);
        System.out.println("Double : " + d3);
        System.out.println("Int    : " + i3);
    }
}
