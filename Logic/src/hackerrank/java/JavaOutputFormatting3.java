package hackerrank.java;

import java.util.Scanner;

public class JavaOutputFormatting3 {

	public static void main(String[] args) {
        Scanner sc3=new Scanner(System.in);
        String[] kata3=new String[3];
        int[] angka3=new int[3];
        for (int i = 0; i < 3; i++) {
        	System.out.print("Masukkan text : ");
        	kata3[i]=sc3.next();
        	System.out.print("Masukkan angka 0-999 : ");
            angka3[i]=sc3.nextInt();
		}
        System.out.println("================================");
        for(int i = 0; i < 3 ; i++) {
            System.out.printf("%-15s%s %n",kata3[i],String.format("%03d",angka3[i]));
        }
        System.out.println("================================");
	}
}
