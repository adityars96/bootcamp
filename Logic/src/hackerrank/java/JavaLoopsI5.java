package hackerrank.java;

import java.util.Scanner;

public class JavaLoopsI5 {

	public static void main(String[] args) {
		Scanner scan5=new Scanner(System.in);
		System.out.print("Masukkan angka : ");
		int N5=scan5.nextInt();
		int kali5=0;
		for (int i = 1; i <= 10; i++) {
			kali5=N5*i;
			System.out.println(N5+" x "+i+" = "+kali5);
		}
	}
}
