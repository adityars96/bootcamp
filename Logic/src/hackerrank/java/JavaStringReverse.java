package hackerrank.java;

import java.util.*;

public class JavaStringReverse {
	public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.print("Masukkan text : ");
        String kata=sc.next(); //input text
        System.out.println("Apakah '"+kata+"' adalah sebuah palindrom?");
        String result=kata.equals(new StringBuilder(kata).reverse().toString())?"Yes":"No"; //mencocokan kata dengan kata.reverse
        System.out.print(result);
    }
}
