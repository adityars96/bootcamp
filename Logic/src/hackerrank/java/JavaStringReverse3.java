package hackerrank.java;

import java.util.Scanner;

public class JavaStringReverse3 {
	public static void main(String[] args) {
        Scanner sc3=new Scanner(System.in);
        System.out.print("Masukkan text : ");
        String kata3=sc3.next();
        System.out.println("Apakah '"+kata3+"' adalah sebuah palindrom?");
        String result3=kata3.equals(new StringBuilder(kata3).reverse().toString())?"Yes":"No";
        System.out.print(result3);
    }
}
