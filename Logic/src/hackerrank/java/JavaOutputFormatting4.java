package hackerrank.java;

import java.util.Scanner;

public class JavaOutputFormatting4 {
	public static void main(String[] args) {
        Scanner sc4=new Scanner(System.in);
        String[] kata4=new String[3];
        int[] angka4=new int[3];
        for (int i = 0; i < 3; i++) {
        	System.out.print("Masukkan text : ");
        	kata4[i]=sc4.next();
        	System.out.print("Masukkan angka 0-999 : ");
            angka4[i]=sc4.nextInt();
		}
        System.out.println("================================");
        for(int i = 0; i < 3 ; i++) {
            System.out.printf("%-15s%s %n",kata4[i],String.format("%03d",angka4[i]));
        }
        System.out.println("================================");
	}
}
