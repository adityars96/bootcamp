package hackerrank.java;

import java.util.Scanner;

public class JavaStringReverse2 {
	public static void main(String[] args) {
        Scanner sc2=new Scanner(System.in);
        System.out.print("Masukkan text : ");
        String kata2=sc2.next();
        System.out.println("Apakah '"+kata2+"' adalah sebuah palindrom?");
        String result2=kata2.equals(new StringBuilder(kata2).reverse().toString())?"Yes":"No";
        System.out.print(result2);
    }
	
}
