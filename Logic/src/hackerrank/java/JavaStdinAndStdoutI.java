package hackerrank.java;

import java.util.*;

public class JavaStdinAndStdoutI {
	public static void main(String[] args) {
        Scanner scan = new Scanner(System.in); //panggil scanner
        System.out.print("Masukkan integer 1 : ");
        int a = scan.nextInt(); //input int 1
        System.out.print("Masukkan integer 2 : ");
        int b = scan.nextInt(); //input int 2
        System.out.print("Masukkan integer 3 : ");
        int c = scan.nextInt(); //input int 3
        System.out.println();
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }
}
