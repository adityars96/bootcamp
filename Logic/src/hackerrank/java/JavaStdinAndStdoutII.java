package hackerrank.java;

import java.util.Scanner;

public class JavaStdinAndStdoutII {
	public static void main(String[] args) {
        Scanner scan = new Scanner(System.in); //panggil scanner
        System.out.print("Masukkan integer : ");
        int i = scan.nextInt(); //input integer
        System.out.print("Masukkan double : ");
        double d = scan.nextDouble(); //input double -> pke (,) bukan (.)
        scan.nextLine();
        System.out.print("Masukkan string : ");
        String s = scan.nextLine(); //input string
        System.out.println();
        System.out.println("String : " + s);
        System.out.println("Double : " + d);
        System.out.println("Int    : " + i);
    }
}
