package hackerrank.implementation;

public class BreakingTheRecord3 {
	static int[] breakingRecords3(int[] scores3) {
        int h3=scores3[0]; int l3=scores3[0];
        int[] record3 = new int[]{0,0};
        for(int i=0; i<scores3.length; i++){
            if(scores3[i]>h3){
                h3=scores3[i];
                record3[0]++;
            }
            if(scores3[i]<l3){
                l3=scores3[i];
                record3[1]++;
            }
        }
        return record3;
    }
    public static void main(String[] args){
        int[] score3=new int[] {10,5,20,20,4,5,2,25,1};
        int[] hasil3=breakingRecords3(score3);
        System.out.println("Breaking Records");
        System.out.println("Best  : "+hasil3[0]);
        System.out.println("Worst : "+hasil3[1]);
    }
}
