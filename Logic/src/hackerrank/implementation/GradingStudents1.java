package hackerrank.implementation;

public class GradingStudents1 {
	static int[] gradingStudents1(int[] grades1) {
		for (int i = 0; i < grades1.length; i++) {
			if (grades1[i]>=38) {
				if (grades1[i]%5>=3) {
					grades1[i]+=(5-grades1[i]%5);
				}
			}
		}
		return grades1;
	}
	
	public static void main(String[] args) {
		int[] grades1=new int[] {73,67,38,33};
		for (int nilai1 : gradingStudents1(grades1)) {
			System.out.println(nilai1);
		}
	}
}
