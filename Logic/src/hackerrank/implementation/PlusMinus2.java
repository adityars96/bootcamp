package hackerrank.implementation;

public class PlusMinus2 {
	static float[] plusMinus2(int[] arr2) {
		float[] count2=new float[arr2.length];
		for (int i = 0; i < arr2.length; i++) {
			if (arr2[i]>0) {
				count2[0]++;				
			} else if(arr2[i]<0) {
				count2[1]++;
			} else {
				count2[2]++;
			}
		}
		float[] result2=new float[count2.length];
		for (int j = 0; j < result2.length; j++) {
			result2[j]=count2[j]/arr2.length;
		}
		return result2;
	}
	
	public static void main(String[] args) {
		int[] arr2=new int[] {-4,3,-9,0,4,1};
		float[] hasil2=plusMinus2(arr2);
		System.out.printf("Ratio pos : %.6f \n",hasil2[0]);
    	System.out.printf("Ratio neg : %.6f \n",hasil2[1]);
    	System.out.printf("Ratio nol : %.6f \n",hasil2[2]);
	}
}
