package hackerrank.implementation;

import java.util.ArrayList;
import java.util.List;

public class CompareTheTriplets1 {
	static List<Integer> compareTriplets1(List<Integer> a1, List<Integer> b1){
		List<Integer> result1=new ArrayList<Integer>();
		result1.add(0);
		result1.add(0);
		int nAlice1=0;
		int nBob1=0;
		for (int i = 0; i < a1.size(); i++) {
			if(a1.get(i)>b1.get(i)) {
				nAlice1++;
				result1.set(0, nAlice1);
			} if(a1.get(i)<b1.get(i)) {
				nBob1++;
				result1.set(1, nBob1);
			}
		} return result1;
	}
	public static void main(String[] args) {
		List<Integer> a1=new ArrayList<Integer>();
		a1.add(17); a1.add(28); a1.add(30);
		
		List<Integer> b1=new ArrayList<Integer>();
		b1.add(99); b1.add(16); b1.add(20);
		
		for (Integer item1 : compareTriplets1(a1, b1)) {
			System.out.print(item1+"\t");
		}
	}
}
