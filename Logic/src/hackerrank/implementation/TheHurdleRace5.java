package hackerrank.implementation;

public class TheHurdleRace5 {
	static int hurdleRace5(int k5, int[] height5) {
		int max5=0; int potion5=0;
		for (int i = 0; i < height5.length; i++) {
			if(height5[i]>max5) {
				max5=height5[i];
			}
		} if(max5>k5) {
			potion5=max5-k5;
		} return potion5;
	}
	public static void main(String[] args) {
		int k5=4;
		int[] height5=new int[] {1,6,3,5,2};
		int hasil5=hurdleRace5(k5, height5);
		System.out.println("Potion : "+hasil5);
	}
}
