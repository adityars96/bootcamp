package hackerrank.implementation;

public class DivisibleSumPairs2 {
	static int divisibleSumPairs2(int n2, int k2, int[] ar2) {
		int count2=0;
		for (int i = 0; i < n2; i++) {
			for (int j = n2-1; j > i; j--) {
				if((ar2[i]+ar2[j])%k2==0)
					count2++;
			}
		} 
		return count2;
	}
	
	public static void main(String[] args) {
		int n2=6; int k2=3;
		int[] ar2=new int[] {1,3,2,6,1,2};
		int hasil2=divisibleSumPairs2(n2, k2, ar2);
		System.out.print("Divisible sum pairs : "+hasil2);
	}
}
