package hackerrank.implementation;

import java.util.*;

public class CompareTheTriplets4 {
	static List<Integer> compareTriplets4(List<Integer> a4, List<Integer> b4){
		List<Integer> result4=new ArrayList<Integer>();
		result4.add(0);	result4.add(0);
		int nAlice4=0; int nBob4=0;
		for (int i = 0; i < a4.size(); i++) {
			if(a4.get(i)>b4.get(i)) {
				nAlice4++;
				result4.set(0, nAlice4);
			} if(a4.get(i)<b4.get(i)) {
				nBob4++;
				result4.set(1, nBob4);
			}
		} 
		return result4;
	}
	public static void main(String[] args) {
		List<Integer> a4=new ArrayList<Integer>();
		a4.add(17); a4.add(28); a4.add(30);
		
		List<Integer> b4=new ArrayList<Integer>();
		b4.add(99); b4.add(16); b4.add(20);
		
		for (Integer item4 : compareTriplets4(a4, b4)) {
			System.out.print(item4+"\t");
		}
	}
}
