package hackerrank.implementation;

public class GradingStudents4 {
	static int[] gradingStudents4(int[] grades4) {
		for (int i = 0; i < grades4.length; i++) {
			if (grades4[i]>=38) {
				if (grades4[i]%5>=3) {
					grades4[i]+=(5-grades4[i]%5);
				}
			}
		} return grades4;
	}
	public static void main(String[] args) {
		int[] grades4=new int[] {73,67,38,33};
		for (int nilai4 : gradingStudents4(grades4)) {
			System.out.println(nilai4);
		}
	}
}
