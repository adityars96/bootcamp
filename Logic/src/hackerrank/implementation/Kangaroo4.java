package hackerrank.implementation;

public class Kangaroo4 {
	static String kangaroo4(int x14, int v14, int x24, int v24) {
		String result4="YES";
		if(x14<x24 && v14<v24) {
			result4="NO";
		} else {
			if(v14!=v24 && (x24-x14)%(v14-v24)==0) {
				result4="YES";
			} else {
				result4="NO";
			}
		} return result4;
	}
	public static void main(String[] args) {
		int x14=0; int v14=3;
		int x24=4; int v24=2;
		String hasil4=kangaroo4(x14, v14, x24, v24);
		System.out.println(hasil4);
	}
}
