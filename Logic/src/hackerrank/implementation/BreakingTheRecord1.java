package hackerrank.implementation;

public class BreakingTheRecord1 {
	static int[] breakingRecords1(int[] scores1) {
        int h1=scores1[0]; int l1=scores1[0];
        int[] record1 = new int[]{0,0};
        for(int i=0; i<scores1.length; i++){
            if(scores1[i]>h1){
                h1=scores1[i];
                record1[0]++;
            }
            if(scores1[i]<l1){
                l1=scores1[i];
                record1[1]++;
            }
        }
        return record1;
    }

    public static void main(String[] args){
        int[] score1=new int[] {10,5,20,20,4,5,2,25,1};
        int[] hasil1=breakingRecords1(score1);
        System.out.println("Breaking Records");
        System.out.println("Best  : "+hasil1[0]);
        System.out.println("Worst : "+hasil1[1]);
    }
}
