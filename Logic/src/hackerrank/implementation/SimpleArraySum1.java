package hackerrank.implementation;

public class SimpleArraySum1 {
	static int simpleArraySum1(int[] ar1) {
		int sum1=0;
		for (int i = 0; i < ar1.length; i++) {
			sum1+=ar1[i];
		}
		return sum1;
	}
	public static void main(String[] args) {
		int[] ar1=new int[] {1,2,3,4,10,11};
		int hasil1=simpleArraySum1(ar1);
		System.out.print("Sum : "+hasil1);
	}
	
}
