package hackerrank.implementation;

public class SimpleArraySum5 {
	static int simpleArraySum5(int[] ar5) {
		int sum5=0;
		for (int i = 0; i < ar5.length; i++) {
			sum5+=ar5[i];
		}return sum5;
	}
	public static void main(String[] args) {
		int[] ar5=new int[] {1,2,3,4,10,11};
		int hasil5=simpleArraySum5(ar5);
		System.out.print("Sum : "+hasil5);
	}
}
