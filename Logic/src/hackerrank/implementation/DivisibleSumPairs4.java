package hackerrank.implementation;

public class DivisibleSumPairs4 {
	static int divisibleSumPairs4(int n4, int k4, int[] ar4) {
		int count4=0;
		for (int i = 0; i < n4; i++) {
			for (int j = n4-1; j > i; j--) {
				if((ar4[i]+ar4[j])%k4==0)
					count4++;
			}
		} return count4;
	}
	public static void main(String[] args) {
		int n4=6; int k4=3;
		int[] ar4=new int[] {1,3,2,6,1,2};
		int hasil4=divisibleSumPairs4(n4, k4, ar4);
		System.out.print("Divisible sum pairs : "+hasil4);
	}
	
}
