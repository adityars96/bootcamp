package hackerrank.implementation;

public class GradingStudents3 {
	static int[] gradingStudents3(int[] grades3) {
		for (int i = 0; i < grades3.length; i++) {
			if (grades3[i]>=38) {
				if (grades3[i]%5>=3) {
					grades3[i]+=(5-grades3[i]%5);
				}
			}
		} return grades3;
	}
	public static void main(String[] args) {
		int[] grades3=new int[] {73,67,38,33};
		for (int nilai3 : gradingStudents3(grades3)) {
			System.out.println(nilai3);
		}
	}
}
