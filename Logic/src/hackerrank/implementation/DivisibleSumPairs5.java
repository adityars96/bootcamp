package hackerrank.implementation;

public class DivisibleSumPairs5 {
	static int divisibleSumPairs5(int n5, int k5, int[] ar5) {
		int count5=0;
		for (int i = 0; i < n5; i++) {
			for (int j = n5-1; j > i; j--) {
				if((ar5[i]+ar5[j])%k5==0)
					count5++;
			}
		} return count5;
	}
	public static void main(String[] args) {
		int n5=6; int k5=3;
		int[] ar5=new int[] {1,3,2,6,1,2};
		int hasil5=divisibleSumPairs5(n5, k5, ar5);
		System.out.print("Divisible sum pairs : "+hasil5);
	}
}
