package hackerrank.implementation;

public class AVeryBigSum5 {
	static long aVeryBigSum5(long[] ar5) {
		long sum5=0;
		for (int i = 0; i < ar5.length; i++) {
			sum5+=ar5[i];
		}
		return sum5;
	}
	public static void main(String[] args) {
		long[] ar5=new long[]{1000000001, 1000000002, 1000000003, 1000000004, 1000000005};
		long hasil5=aVeryBigSum5(ar5);
		System.out.print("Sum = "+hasil5);
	}
}
