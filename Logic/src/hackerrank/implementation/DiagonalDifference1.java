package hackerrank.implementation;

public class DiagonalDifference1 {
	static int diagonalDifference(int[][] arr1) {
		int kanan1=0;
		int kiri1=0;
		for (int i = 0; i < arr1.length; i++) {
			kanan1+=arr1[i][i];
			kiri1+=arr1[arr1.length-1-i][i];
		}
		int result1=Math.abs(kanan1-kiri1);
		return result1;
	}
	public static void main(String[] args) {
		int[][] arr1=new int[][] {{11,2,4},{4,5,6},{10,8,-12}};
		int hasil1=diagonalDifference(arr1);
		System.out.println("Diagonal difference : "+hasil1);
	}
}
