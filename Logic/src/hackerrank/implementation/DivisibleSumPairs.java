package hackerrank.implementation;

public class DivisibleSumPairs {
	static int divisibleSumPairs(int n, int k, int[] ar) {
        int count=0; //var untuk menghitung divisible sum pairs
        for(int i=0; i<n; i++){
            for(int j=n-1; j>i; j--){
                if((ar[i]+ar[j])%k==0){ //jika (ar[i]+ar[j])%k=0 maka count +1
                    count++;
                }
            }
        }
        return count;

    }

    public static void main(String[] args){
        int n=6; //n->banyak array ar
        int k=3; //k->pembagi
        int[] ar=new int[] {1,3,2,6,1,2}; //membuat array ar
        int hasil=divisibleSumPairs(n, k, ar); //memanggil method divisibleSumPairs
        System.out.print("Divisible sum pairs : "+hasil);
    }
}
