package hackerrank.implementation;

import java.util.*;

public class BetweenTwoSets {
	static int betweenTwoSets(int[] a, int[] b) {
		int count=0;
		List<Integer> list=new ArrayList<Integer>();
		int min=b[0];
		for (int i = 0; i < b.length; i++) {
			if(b[i]<min) {
				min=b[i];
			}
		}
		for (int j = 1; j <= min; j++) {
			int cek=0;
			for (int k = 0; k < b.length; k++) {
				if(b[k]%j!=0) {
					cek++;
				}
			}
			if(cek==0) {
				list.add(j);
			}
		}
		for (int l = 0; l < list.size(); l++) {
			int cek=0;
			for (int m = 0; m < a.length; m++) {
				if(list.get(l)%a[m]!=0) {
					cek++;
				}
			}
			if(cek==0) {
				count++;
			}
		}
		
		return count;
	}
	public static void main(String[] args) {
		int[] a=new int[] {2,4};
		int[] b=new int[] {16,32,96};
		System.out.print(betweenTwoSets(a, b));
	}
}
