package hackerrank.implementation;

public class PlusMinus1 {
	static float[] plusMinus1(int[] arr1) {
		float[] count1=new float[arr1.length];
		for (int i = 0; i < arr1.length; i++) {
			if (arr1[i]>0) {
				count1[0]++;				
			} else if(arr1[i]<0) {
				count1[1]++;
			} else {
				count1[2]++;
			}
		}
		float[] result1=new float[count1.length];
		for (int j = 0; j < result1.length; j++) {
			result1[j]=count1[j]/arr1.length;
		}
		return result1;
	}
	
	public static void main(String[] args) {
		int[] arr1=new int[] {-4,3,-9,0,4,1};
		float[] hasil1=plusMinus1(arr1);
		System.out.printf("Ratio pos : %.6f \n",hasil1[0]);
    	System.out.printf("Ratio neg : %.6f \n",hasil1[1]);
    	System.out.printf("Ratio nol : %.6f \n",hasil1[2]);
	}
	
}
