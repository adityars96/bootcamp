package hackerrank.implementation;

public class AVeryBigSum {
	static long aVeryBigSum(long[] ar) {
        long sum=0; //nilai sum awal
        for(int i=0;i<ar.length;i++){
            sum+=ar[i]; //update nilai sum dengan elemen ar ke-i
        }
        return sum;
    }
    public static void main(String[] args){
    	long[] ar=new long[] {1000000001, 1000000002, 1000000003, 1000000004, 1000000005}; //membuat array ar
    	long hasil=aVeryBigSum(ar); //memanggil aVeryBigSum
    	System.out.print("Sum = "+hasil);
    }
}
