package hackerrank.implementation;

public class GradingStudents {
	static int[] gradingStudents(int[] grades) {
        for(int i=0;i<grades.length;i++){
            if(grades[i]>=38){ //jika nilai >=38 maka ...
                if(grades[i]%5>=3){ //jika nilai%5 >=3 maka ...
                    grades[i]=grades[i]+(5-grades[i]%5); //nilai ditambah kurangnya (nilai dibulatkan)
                }
            }
        }
        return grades;

    }

    public static void main(String[] args) {
        int[] grades=new int[] {73,67,38,33}; //membuat array nilai
        for (int nilai : gradingStudents(grades)) { //memanggil method gradingStudents
			System.out.println(nilai);
		}
    }
}
