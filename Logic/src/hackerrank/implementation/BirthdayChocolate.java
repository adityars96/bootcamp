package hackerrank.implementation;

import java.util.List;

public class BirthdayChocolate {
	static int birthdayChocolate(int[] s, int d, int m) {
		int count=0;
		for (int i = 0; i <= s.length-m; i++) {
			int total=0;
			for (int j = 0; j < m; j++) {
				total+=s[i+j];
			}
			if(total==d) {
				count++;
			}
		} return count;
	}
	public static void main(String[] args) {
		int[] s=new int[] {1,1,1,1,1};
		System.out.println(birthdayChocolate(s, 2, 2));
	}
}
