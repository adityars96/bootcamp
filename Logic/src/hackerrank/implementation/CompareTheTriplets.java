package hackerrank.implementation;

import java.util.*;

public class CompareTheTriplets {
    static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
        List<Integer> result=new ArrayList<Integer>(); //membuat list kosong
        result.add(0); //menambah index list 0->1
        result.add(0); //menambah index list 1->2
        int nAlice=0; //n Alice
        int nBob=0;  //n Bob
        for(int i=0;i<a.size();i++){
            if(a.get(i)>b.get(i)){ //menambah n Alice
                nAlice++;
                result.set(0,nAlice);
            } if(a.get(i)<b.get(i)){ //menambah n Bob
                nBob++;
                result.set(1, nBob);
            }
        }
        return result;

    }

    public static void main(String[] args) {
        List<Integer> a=new ArrayList<Integer>(); //a -> Alice
        a.add(17); a.add(28); a.add(30);
        
        List<Integer> b=new ArrayList<Integer>(); //b -> Bob
        b.add(99); b.add(16); b.add(20);
        
        for (Integer item : compareTriplets(a, b)) {
			System.out.print(item+"\t");
		}
    }
}