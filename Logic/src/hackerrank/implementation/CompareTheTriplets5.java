package hackerrank.implementation;

import java.util.ArrayList;
import java.util.List;

public class CompareTheTriplets5 {
	static List<Integer> compareTriplets5(List<Integer> a5, List<Integer> b5){
		List<Integer> result5=new ArrayList<Integer>();
		result5.add(0); result5.add(0);
		int nAlice5=0; int nBob5=0;
		for (int i = 0; i < a5.size(); i++) {
			if(a5.get(i)>b5.get(i)) {
				nAlice5++;
				result5.set(0, nAlice5);
			} if(a5.get(i)<b5.get(i)) {
				nBob5++;
				result5.set(1, nBob5);
			}
		} return result5;
	}
	
	public static void main(String[] args) {
		List<Integer> a5=new ArrayList<Integer>();
		a5.add(17); a5.add(28); a5.add(30);
		
		List<Integer> b5=new ArrayList<Integer>();
		b5.add(99); b5.add(16); b5.add(20);
		
		for (Integer item5 : compareTriplets5(a5, b5)) {
			System.out.print(item5+"\t");
		}
	}
}
