package hackerrank.implementation;

public class TheHurdleRace4 {
	static int hurdleRace4(int k4, int[] height4) {
		int max4=0; int potion4=0;
		for (int i = 0; i < height4.length; i++) {
			if(height4[i]>max4) {
				max4=height4[i];
			}
		} if(max4>k4) {
			potion4=max4-k4;
		} return potion4;
	}
	public static void main(String[] args) {
		int k4=4;
		int[] height4=new int[] {1,6,3,5,2};
		int hasil4=hurdleRace4(k4, height4);
		System.out.println("Potion : "+hasil4);
	}
}
