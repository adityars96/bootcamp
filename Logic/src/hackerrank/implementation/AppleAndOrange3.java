package hackerrank.implementation;

public class AppleAndOrange3 {
	static int[] countApplesAndOranges3(int s3, int t3, int a3, int b3, int[] apples3, int[] oranges3) {
		int[] hasil3=new int[2];
		for (int i = 0; i < apples3.length; i++) {
			apples3[i]=a3+apples3[i];
			if (apples3[i]>=s3 && apples3[i]<=t3) {
				hasil3[0]++;
			}
		} for (int j = 0; j < oranges3.length; j++) {
			oranges3[j]=b3+oranges3[j];
			if(oranges3[j]>=s3 && oranges3[j]<=t3) {
				hasil3[1]++;
			}
		} return hasil3;
	}
	public static void main(String[] args) {
		int s3=7; int t3=11;
		int a3=5; int b3=15;
		int[] apples3=new int[] {-2,2,1};
		int[] oranges3=new int[] {5,-6};
		int[] hasil3=countApplesAndOranges3(s3, t3, a3, b3, apples3, oranges3);
		System.out.println("Apel  : "+hasil3[0]);
		System.out.println("Jeruk : "+hasil3[1]);
	}
}
