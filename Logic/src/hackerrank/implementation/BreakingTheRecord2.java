package hackerrank.implementation;

public class BreakingTheRecord2 {
	static int[] breakingRecords2(int[] scores2) {
        int h2=scores2[0]; int l2=scores2[0];
        int[] record2 = new int[]{0,0};
        for(int i=0; i<scores2.length; i++){
            if(scores2[i]>h2){
                h2=scores2[i];
                record2[0]++;
            } if(scores2[i]<l2){
                l2=scores2[i];
                record2[1]++;
            }
        } return record2;
    }

    public static void main(String[] args){
        int[] score2=new int[] {10,5,20,20,4,5,2,25,1};
        int[] hasil2=breakingRecords2(score2);
        System.out.println("Breaking Records");
        System.out.println("Best  : "+hasil2[0]);
        System.out.println("Worst : "+hasil2[1]);
    }
}
