package hackerrank.implementation;

public class Kangaroo {
	static String kangaroo(int x1, int v1, int x2, int v2) {
        String result="YES";
        if(x1<x2 && v1<v2){ //jika posisi dan laju kanguru 1 lebih unggul daripada kanguru 2 maka NO
            result="NO";
        } else{
            if(v1!=v2 && (x2-x1)%(v1-v2)==0){ //jika laju tidak sama dan percepatan=>0 maka YES
                result="YES";
            } else{
                result="NO";
            }
        }
        return result;
    }

    public static void main(String[] args){
        int x1=0; int v1=3; //posisi & laju kanguru 1
        int x2=4; int v2=2; //posisi & laju kanguru 2
        String hasil=kangaroo(x1, v1, x2, v2);
        System.out.println(hasil);
    }
}
