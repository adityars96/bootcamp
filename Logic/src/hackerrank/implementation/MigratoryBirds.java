package hackerrank.implementation;

public class MigratoryBirds {
	static int migratoryBirds(int[] arr) {
		int[] type=new int[6];
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < type.length; j++) {
				if(arr[i]==j) {
					type[j]++;
				}
			}
		}
		int max=type[0]; int hasil=0;
		for (int k = 0; k < type.length; k++) {
			if(type[k]>max) {
				max=type[k];
				hasil=k;
			}
		}
		return hasil;
	}
	public static void main(String[] args) {
		int[] arr=new int[] {1,2,3,4,5,4,3,2,1,3,5,5,5};
		System.out.println(migratoryBirds(arr));
	}
}
