package hackerrank.implementation;

import java.util.ArrayList;
import java.util.List;

public class CompareTheTriplets3 {
	static List<Integer> compareTriplets3(List<Integer> a3, List<Integer> b3){
		List<Integer> result3=new ArrayList<Integer>();
		result3.add(0);	result3.add(0);
		int nAlice3=0; int nBob3=0;
		for (int i = 0; i < a3.size(); i++) {
			if(a3.get(i)>b3.get(i)) {
				nAlice3++;
				result3.set(0, nAlice3);
			} if(a3.get(i)<b3.get(i)) {
				nBob3++;
				result3.set(1, nBob3);
			}
		} 
		return result3;
	}
	
	public static void main(String[] args) {
		List<Integer> a3=new ArrayList<Integer>();
		a3.add(17); a3.add(28); a3.add(30);
		
		List<Integer> b3=new ArrayList<Integer>();
		b3.add(99); b3.add(16); b3.add(20);
		
		for (Integer item3 : compareTriplets3(a3, b3)) {
			System.out.print(item3+"\t");
		}
	}
}
