package hackerrank.implementation;

public class BreakingTheRecord5 {
	static int[] breakingRecords5(int[] scores5) {
        int h5=scores5[0]; int l5=scores5[0];
        int[] record5 = new int[]{0,0};
        for(int i=0; i<scores5.length; i++){
            if(scores5[i]>h5){
                h5=scores5[i];
                record5[0]++;
            }
            if(scores5[i]<l5){
                l5=scores5[i];
                record5[1]++;
            }
        }
        return record5;
    }
    public static void main(String[] args){
        int[] score5=new int[] {10,5,20,20,4,5,2,25,1};
        int[] hasil5=breakingRecords5(score5);
        System.out.println("Breaking Records");
        System.out.println("Best  : "+hasil5[0]);
        System.out.println("Worst : "+hasil5[1]);
    }
}
