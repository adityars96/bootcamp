package hackerrank.implementation;

public class DiagonalDifference3 {
	static int diagonalDifference(int[][] arr3) {
		int kanan3=0; int kiri3=0;
		for (int i = 0; i < arr3.length; i++) {
			kanan3+=arr3[i][i];
			kiri3+=arr3[arr3.length-1-i][i];
		}
		int result3=Math.abs(kanan3-kiri3);
		return result3;
	}
	
	public static void main(String[] args) {
		int[][] arr3=new int[][] {{11,2,4},{4,5,6},{10,8,-12}};
		int hasil3=diagonalDifference(arr3);
		System.out.println("Diagonal difference : "+hasil3);
	}
}
