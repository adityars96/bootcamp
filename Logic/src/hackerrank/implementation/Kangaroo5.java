package hackerrank.implementation;

public class Kangaroo5 {
	static String kangaroo5(int x15, int v15, int x25, int v25) {
		String result5="YES";
		if(x15<x25 && v15<v25) {
			result5="NO";
		} else {
			if(v15!=v25 && (x25-x15)%(v15-v25)==0) {
				result5="YES";
			} else {
				result5="NO";
			}
		} return result5;
	}
	public static void main(String[] args) {
		int x15=0; int v15=3;
		int x25=4; int v25=2;
		String hasil5=kangaroo5(x15, v15, x25, v25);
		System.out.println(hasil5);
	}
	
}
