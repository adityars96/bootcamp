package hackerrank.implementation;

public class TheHurdleRace1 {
	static int hurdleRace1(int k1, int[] height1) {
		int max1=0;
		for (int i = 0; i < height1.length; i++) {
			if(height1[i]>max1) {
				max1=height1[i];
			}
		}
		int potion1=0;
		if(max1>k1) {
			potion1=max1-k1;
		}
		return potion1;
	}
	
	public static void main(String[] args) {
		int k1=4;
		int[] height1=new int[] {1,6,3,5,2};
		int hasil1=hurdleRace1(k1, height1);
		System.out.println("Potion : "+hasil1);
	}
}
