package hackerrank.implementation;

public class SimpleArraySum3 {
	static int simpleArraySum3(int[] ar3) {
		int sum3=0;
		for (int i = 0; i < ar3.length; i++) {
			sum3+=ar3[i];
		} return sum3;
		
	}
	public static void main(String[] args) {
		int[] ar3=new int[] {1,2,3,4,10,11};
		int hasil3=simpleArraySum3(ar3);
		System.out.print("Sum : "+hasil3);
	}
}
