package hackerrank.implementation;

public class AppleAndOrange2 {
	static int[] countApplesAndOranges2(int s2, int t2, int a2, int b2, int[] apples2, int[] oranges2) {
		int[] hasil2=new int[2];
		for (int i = 0; i < apples2.length; i++) {
			apples2[i]=a2+apples2[i];
			if(apples2[i]>=s2 && apples2[i]<=t2) {
				hasil2[0]++;
			}
		} for (int j = 0; j < oranges2.length; j++) {
			oranges2[j]=b2+oranges2[j];
			if(oranges2[j]>=s2 && oranges2[j]<=t2) {
				hasil2[1]++;
			}
		} return hasil2;
	}
	public static void main(String[] args) {
		int s2=7; int t2=11;
		int a2=5; int b2=15;
		int[] apples2=new int[] {-2,2,1};
		int[] oranges2=new int[] {5,-6};
		int[] hasil2=countApplesAndOranges2(s2, t2, a2, b2, apples2, oranges2);
		System.out.println("Apel  : "+hasil2[0]);
		System.out.println("Jeruk : "+hasil2[1]);
	}
}
