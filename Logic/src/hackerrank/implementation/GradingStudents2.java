package hackerrank.implementation;

public class GradingStudents2 {
	static int[] gradingStudents2(int[] grades2) {
		for (int i = 0; i < grades2.length; i++) {
			if (grades2[i]>=38) {
				if (grades2[i]%5>=3) {
					grades2[i]+=(5-grades2[i]%5);
				}
			}
		} 
		return grades2;
	}
	
	public static void main(String[] args) {
		int[] grades2=new int[] {73,67,38,33};
		for (int nilai2 : gradingStudents2(grades2)) {
			System.out.println(nilai2);
		}
	}
}
