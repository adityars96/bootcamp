package hackerrank.implementation;

public class TheHurdleRace3 {
	static int hurdleRace3(int k3, int[] height3) {
		int max3=0; int potion3=0;
		for (int i = 0; i < height3.length; i++) {
			if(height3[i]>max3) {
				max3=height3[i];
			}
		} if(max3>k3) {
			potion3=max3-k3;
		}return potion3;
	}
	public static void main(String[] args) {
		int k3=4;
		int[] height3=new int[] {1,6,3,5,2};
		int hasil3=hurdleRace3(k3, height3);
		System.out.println("Potion : "+hasil3);
	}
}
