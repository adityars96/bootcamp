package hackerrank.implementation;

public class AVeryBigSum4 {
	static long aVeryBigSum4(long[] ar4) {
		long sum4=0;
		for (int i = 0; i < ar4.length; i++) {
			sum4+=ar4[i];
		} return sum4;
	}
	
	public static void main(String[] args) {
		long[] ar4=new long[]{1000000001, 1000000002, 1000000003, 1000000004, 1000000005};
		long hasil4=aVeryBigSum4(ar4);
		System.out.print("Sum = "+hasil4);
	}
}
