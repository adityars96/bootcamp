package hackerrank.implementation;

public class DiagonalDifference2 {
	static int diagonalDifference(int[][] arr2) {
		int kanan2=0; int kiri2=0;
		for (int i = 0; i < arr2.length; i++) {
			kanan2+=arr2[i][i];
			kiri2+=arr2[arr2.length-1-i][i];
		}
		int result2=Math.abs(kanan2-kiri2);
		return result2;
	}
	
	public static void main(String[] args) {
		int[][] arr2=new int[][] {{11,2,4},{4,5,6},{10,8,-12}};
		int hasil2=diagonalDifference(arr2);
		System.out.println("Diagonal difference : "+hasil2);
	}
}
