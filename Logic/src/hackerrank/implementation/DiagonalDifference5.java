package hackerrank.implementation;

public class DiagonalDifference5 {
	static int diagonalDifference(int[][] arr5) {
		int kanan5=0; int kiri5=0;
		for (int i = 0; i < arr5.length; i++) {
			kanan5+=arr5[i][i];
			kiri5+=arr5[arr5.length-1-i][i];
		} int result5=Math.abs(kanan5-kiri5);
		return result5;
	}
	public static void main(String[] args) {
		int[][] arr5=new int[][] {{11,2,4},{4,5,6},{10,8,-12}};
		int hasil5=diagonalDifference(arr5);
		System.out.println("Diagonal difference : "+hasil5);
	}
}
