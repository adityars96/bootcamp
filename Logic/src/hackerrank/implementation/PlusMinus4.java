package hackerrank.implementation;

public class PlusMinus4 {
	static float[] plusMinus4(int[] arr4) {
		float[] count4=new float[arr4.length];
		for (int i = 0; i < arr4.length; i++) {
			if (arr4[i]>0) {
				count4[0]++;				
			} else if(arr4[i]<0) {
				count4[1]++;
			} else {
				count4[2]++;
			}
		}
		float[] result4=new float[count4.length];
		for (int j = 0; j < result4.length; j++) {
			result4[j]=count4[j]/arr4.length;
		}
		return result4;
	}
	public static void main(String[] args) {
		int[] arr4=new int[] {-4,3,-9,0,4,1};
		float[] hasil4=plusMinus4(arr4);
		System.out.printf("Ratio pos : %.6f \n",hasil4[0]);
    	System.out.printf("Ratio neg : %.6f \n",hasil4[1]);
    	System.out.printf("Ratio nol : %.6f \n",hasil4[2]);
	}
}
