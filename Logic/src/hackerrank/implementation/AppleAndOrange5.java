package hackerrank.implementation;

public class AppleAndOrange5 {
	static int[] countApplesAndOranges5(int s5, int t5, int a5, int b5, int[] apples5, int[] oranges5) {
		int[] hasil5=new int[2];
		for (int i = 0; i < apples5.length; i++) {
			apples5[i]=a5+apples5[i];
			if(apples5[i]>=s5 && apples5[i]<=t5) {
				hasil5[0]++;
			}
		}
		for (int j = 0; j < oranges5.length; j++) {
			oranges5[j]=b5+oranges5[j];
			if(oranges5[j]>=s5 && oranges5[j]<=t5) {
				hasil5[1]++;
			}
		}
		return hasil5;
	}
	public static void main(String[] args) {
		int s5=7; int t5=11;
		int a5=5; int b5=15;
		int[] apples5=new int[] {-2,2,1};
		int[] oranges5=new int[] {5,-6};
		int[] hasil5=countApplesAndOranges5(s5, t5, a5, b5, apples5, oranges5);
		System.out.println("Apel  : "+hasil5[0]);
		System.out.println("Jeruk : "+hasil5[1]);
	}
}
