package hackerrank.implementation;

public class DiagonalDifference {
	static int diagonalDifference(int[][] arr) {
        int kanan=0; //jumlah diagonal kanan
        int kiri=0; //jumlah diagonal kiri
        for(int i=0;i<arr.length;i++){
            kanan+=arr[i][i];
            kiri+=arr[arr.length-1-i][i];
        }
        int result=Math.abs(kanan-kiri);
        return result;

    }

    public static void main(String[] args){
        int[][] arr=new int[][] {{11,2,4},{4,5,6},{10,8,-12}}; //membuat matriks
        int hasil=diagonalDifference(arr); //memanggil method diagonalDifference
        System.out.println("Diagonal difference : "+hasil);
    }
}
