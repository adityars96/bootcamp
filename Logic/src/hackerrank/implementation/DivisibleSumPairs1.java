package hackerrank.implementation;

public class DivisibleSumPairs1 {
	static int divisibleSumPairs1(int n1, int k1, int[] ar1) {
		int count1=0;
		for (int i = 0; i < n1; i++) {
			for (int j = n1-1; j > i; j--) {
				if((ar1[i]+ar1[j])%k1==0)
					count1++;
			}
		} 
		return count1;
	}
	public static void main(String[] args) {
		int n1=6; int k1=3;
		int[] ar1=new int[] {1,3,2,6,1,2};
		int hasil1=divisibleSumPairs1(n1, k1, ar1);
		System.out.print("Divisible sum pairs : "+hasil1);
	}
	
}
