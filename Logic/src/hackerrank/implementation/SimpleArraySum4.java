package hackerrank.implementation;

public class SimpleArraySum4 {
	static int simpleArraySum4(int[] ar4) {
		int sum4=0;
		for (int i = 0; i < ar4.length; i++) {
			sum4+=ar4[i];
		} return sum4;
	}
	public static void main(String[] args) {
		int[] ar4=new int[] {1,2,3,4,10,11};
		int hasil4=simpleArraySum4(ar4);
		System.out.print("Sum : "+hasil4);
	}
}
