package hackerrank.implementation;

public class AVeryBigSum1 {
	static long aVeryBigSum1(long[] ar1) {
		long sum1=0;
		for (int i = 0; i < ar1.length; i++) {
			sum1+=ar1[i];
		} return sum1;
	}
	public static void main(String[] args) {
		long[] ar1=new long[]{1000000001, 1000000002, 1000000003, 1000000004, 1000000005};
		long hasil1=aVeryBigSum1(ar1);
		System.out.print("Sum = "+hasil1);
	}
}
