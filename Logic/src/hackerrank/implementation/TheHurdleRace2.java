package hackerrank.implementation;

public class TheHurdleRace2 {
	static int hurdleRace2(int k2, int[] height2) {
		int max2=0; int potion2=0;
		for (int i = 0; i < height2.length; i++) {
			if(height2[i]>max2) {
				max2=height2[i];
			}
		} if(max2>k2) {
			potion2=max2-k2;
		}
		return potion2;
	}
	public static void main(String[] args) {
		int k2=4;
		int[] height2=new int[] {1,6,3,5,2};
		int hasil2=hurdleRace2(k2, height2);
		System.out.println("Potion : "+hasil2);
	}
}
