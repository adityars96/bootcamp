package hackerrank.implementation;

public class PlusMinus {
	static float[] plusMinus(int[] arr) {
        float[] count=new float[arr.length]; //membuat array count [0]->pos [1]->neg [2]->nol
        for(int i=0;i<arr.length;i++){
            if(arr[i]>0){ //jika pos maka count[0]++
                count[0]++;
            } else if(arr[i]<0){ //jika neg maka count[1]++
                count[1]++;
            } else{ //jika lainnya maka count[2]++
                count[2]++;
            }
        }
        float [] result=new float[count.length]; //membuat array result
        for (int i = 0; i < result.length; i++) {
			result[i]=count[i]/arr.length; //menghitung rationya
		}
        return result;
    }

    public static void main(String[] args) {
    	int[] arr=new int[] {-4,3,-9,0,4,1}; //membuat array arr
    	float[] hasil=plusMinus(arr); //memanggil method plusMinus
    	System.out.printf("Ratio pos : %.6f \n",hasil[0]);
    	System.out.printf("Ratio neg : %.6f \n",hasil[1]);
    	System.out.printf("Ratio nol : %.6f \n",hasil[2]);
    }
}
