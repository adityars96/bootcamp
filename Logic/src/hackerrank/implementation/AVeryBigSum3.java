package hackerrank.implementation;

public class AVeryBigSum3 {
	static long aVeryBigSum3(long[] ar3) {
		long sum3=0;
		for (int i = 0; i < ar3.length; i++) {
			sum3+=ar3[i];
		} return sum3;
	}
	
	public static void main(String[] args) {
		long[] ar3=new long[]{1000000001, 1000000002, 1000000003, 1000000004, 1000000005};
		long hasil3=aVeryBigSum3(ar3);
		System.out.print("Sum = "+hasil3);
	}
}
