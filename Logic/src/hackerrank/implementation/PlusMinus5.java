package hackerrank.implementation;

public class PlusMinus5 {
	static float[] plusMinus5(int[] arr5) {
		float[] count5=new float[arr5.length];
		for (int i = 0; i < arr5.length; i++) {
			if (arr5[i]>0) {
				count5[0]++;				
			} else if(arr5[i]<0) {
				count5[1]++;
			} else {
				count5[2]++;
			}
		}
		float[] result5=new float[count5.length];
		for (int j = 0; j < result5.length; j++) {
			result5[j]=count5[j]/arr5.length;
		}
		return result5;
	}
	public static void main(String[] args) {
		int[] arr5=new int[] {-4,3,-9,0,4,1};
		float[] hasil5=plusMinus5(arr5);
		System.out.printf("Ratio pos : %.6f \n",hasil5[0]);
    	System.out.printf("Ratio neg : %.6f \n",hasil5[1]);
    	System.out.printf("Ratio nol : %.6f \n",hasil5[2]);
	}
	
}
