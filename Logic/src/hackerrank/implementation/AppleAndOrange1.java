package hackerrank.implementation;

public class AppleAndOrange1 {
	static int[] countApplesAndOranges1(int s1, int t1, int a1, int b1, int[] apples1, int[] oranges1) {
		int[] hasil1=new int[2];
		for (int i = 0; i < apples1.length; i++) {
			apples1[i]=a1+apples1[i];
			if (apples1[i]>=s1 && apples1[i]<=t1) {
				hasil1[0]++;
			}
		} for (int j = 0; j < oranges1.length; j++) {
			oranges1[j]=b1+oranges1[j];
			if(oranges1[j]>=s1 && oranges1[j]<=t1) {
				hasil1[1]++;
			}
		} return hasil1;
	}
	
	public static void main(String[] args) {
		int s1=7; int t1=11;
		int a1=5; int b1=15;
		int[] apples1=new int[] {-2,2,1};
		int[] oranges1=new int[] {5,-6};
		int[] hasil1=countApplesAndOranges1(s1, t1, a1, b1, apples1, oranges1);
		System.out.println("Apel  : "+hasil1[0]);
		System.out.println("Jeruk : "+hasil1[1]);
	}
}
