package hackerrank.implementation;

public class BreakingTheRecord4 {
	static int[] breakingRecords4(int[] scores4) {
        int h4=scores4[0]; int l4=scores4[0];
        int[] record4 = new int[]{0,0};
        for(int i=0; i<scores4.length; i++){
            if(scores4[i]>h4){
                h4=scores4[i];
                record4[0]++;
            }
            if(scores4[i]<l4){
                l4=scores4[i];
                record4[1]++;
            }
        } return record4;
    }

    public static void main(String[] args){
        int[] score4=new int[] {10,5,20,20,4,5,2,25,1};
        int[] hasil4=breakingRecords4(score4);
        System.out.println("Breaking Records");
        System.out.println("Best  : "+hasil4[0]);
        System.out.println("Worst : "+hasil4[1]);
    }
}
