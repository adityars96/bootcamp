package hackerrank.implementation;

public class AppleAndOrange {
	static int[] countApplesAndOranges(int s, int t, int a, int b, int[] apples, int[] oranges) {
		int[] hasil=new int[2]; //membuat array hasil(0->apel & 1->jeruk)
        for(int i=0;i<apples.length;i++){
            apples[i]=a+apples[i]; //update lokasi apel
            if (apples[i]>=s && apples[i]<=t){
                hasil[0]++; //menambah jumlah apel yg jatuh dirumah
            }
        }
        for(int j=0;j<oranges.length;j++){
            oranges[j]=b+oranges[j]; //update lokasi jeruk
            if (oranges[j]>=s && oranges[j]<=t){
                hasil[1]++; //menambah jumlah jeruk yg jatuh dirumah
            }
        } return hasil;
    }

    public static void main(String[] args) {
        int s=7; int t=11; //lokasi rumah s-t
        int a=5; int b=15; //lokasi pohon a=apel b=jeruk
        int[] apples=new int[] {-2,2,1}; //jarak jatuh apel dr pohon
        int[] oranges=new int[] {5,-6}; //jarak jatuh jeruk dr pohon
        int[] hasil=countApplesAndOranges(s, t, a, b, apples, oranges); //memanggil countApplesAndOranges 
        System.out.println("Apel  : "+hasil[0]); //print apel
        System.out.println("Jeruk : "+hasil[1]); //print jeruk
    }
}
