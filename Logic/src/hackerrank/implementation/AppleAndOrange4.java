package hackerrank.implementation;

public class AppleAndOrange4 {
	static int[] countApplesAndOranges4(int s4, int t4, int a4, int b4, int[] apples4, int[] oranges4) {
		int[] hasil4=new int[2];
		for (int i = 0; i<apples4.length; i++) {
			apples4[i]=a4+apples4[i];
			if(apples4[i]>=s4 && apples4[i]<=t4) {
				hasil4[0]++;
			}
		}
		for (int j = 0; j<oranges4.length; j++) {
			oranges4[j]=b4+oranges4[j];
			if(oranges4[j]>=s4 && oranges4[j]<=t4) {
				hasil4[1]++;
			}
		} return hasil4;
	}
	public static void main(String[] args) {
		int s4=7; int t4=11;
		int a4=5; int b4=15;
		int[] apples4=new int[] {-2,2,1};
		int[] oranges4=new int[] {5,-6};
		int[] hasil4=countApplesAndOranges4(s4, t4, a4, b4, apples4, oranges4);
		System.out.println("Apel  : "+hasil4[0]);
		System.out.println("Jeruk : "+hasil4[1]);
	}
}
