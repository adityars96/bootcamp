package hackerrank.implementation;

public class Kangaroo3 {
	static String kangaroo3(int x13, int v13, int x23, int v23) {
		String result3="YES";
		if(x13<x23 && v13<v23) {
			result3="NO";
		} else {
			if(v13!=v23 && (x23-x13)%(v13-v23)==0) {
				result3="YES";
			} else {
				result3="NO";
			}
		} return result3;
	}
	public static void main(String[] args) {
		int x13=0; int v13=3;
		int x23=4; int v23=2;
		String hasil3=kangaroo3(x13, v13, x23, v23);
		System.out.println(hasil3);
	}
	
}
