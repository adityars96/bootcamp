package hackerrank.implementation;

public class GradingStudents5 {
	static int[] gradingStudents5(int[] grades5) {
		for (int i = 0; i < grades5.length; i++) {
			if (grades5[i]>=38) {
				if (grades5[i]%5>=3) {
					grades5[i]+=(5-grades5[i]%5);
				}
			}
		} 
		return grades5;
	}
	public static void main(String[] args) {
		int[] grades5=new int[] {73,67,38,33};
		for (int nilai5 : gradingStudents5(grades5)) {
			System.out.println(nilai5);
		}
	}
}
