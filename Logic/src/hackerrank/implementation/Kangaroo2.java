package hackerrank.implementation;

public class Kangaroo2 {
	static String kangaroo2(int x12, int v12, int x22, int v22) {
		String result2="YES";
		if(x12<x22 && v12<v22) {
			result2="NO";
		} else {
			if(v12!=v22 && (x22-x12)%(v12-v22)==0) {
				result2="YES";
			} else {
				result2="NO";
			}
		} return result2;
	}
	
	public static void main(String[] args) {
		int x12=0; int v12=3;
		int x22=4; int v22=2;
		String hasil2=kangaroo2(x12, v12, x22, v22);
		System.out.println(hasil2);
	}
}
