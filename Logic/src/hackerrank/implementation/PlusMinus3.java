package hackerrank.implementation;

public class PlusMinus3 {
	static float[] plusMinus3(int[] arr3) {
		float[] count3=new float[arr3.length];
		for (int i = 0; i < arr3.length; i++) {
			if (arr3[i]>0) {
				count3[0]++;				
			} else if(arr3[i]<0) {
				count3[1]++;
			} else {
				count3[2]++;
			}
		}
		float[] result3=new float[count3.length];
		for (int j = 0; j < result3.length; j++) {
			result3[j]=count3[j]/arr3.length;
		}
		return result3;
	}
	public static void main(String[] args) {
		int[] arr3=new int[] {-4,3,-9,0,4,1};
		float[] hasil3=plusMinus3(arr3);
		System.out.printf("Ratio pos : %.6f \n",hasil3[0]);
    	System.out.printf("Ratio neg : %.6f \n",hasil3[1]);
    	System.out.printf("Ratio nol : %.6f \n",hasil3[2]);
	}
}
