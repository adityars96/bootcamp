package hackerrank.implementation;

import java.util.ArrayList;
import java.util.List;

public class CompareTheTriplets2 {
	static List<Integer> compareTriplets2(List<Integer> a2, List<Integer> b2){
		List<Integer> result2=new ArrayList<Integer>();
		result2.add(0); result2.add(0);
		int nAlice2=0; int nBob2=0;
		for (int i = 0; i < a2.size(); i++) {
			if(a2.get(i)>b2.get(i)) {
				nAlice2++;
				result2.set(0, nAlice2);
			} if(a2.get(i)<b2.get(i)) {
				nBob2++;
				result2.set(1, nBob2);
			}
		} return result2;
	}
	public static void main(String[] args) {
		List<Integer> a2=new ArrayList<Integer>();
		a2.add(17); a2.add(28); a2.add(30);
		
		List<Integer> b2=new ArrayList<Integer>();
		b2.add(99); b2.add(16); b2.add(20);
		
		for (Integer item2 : compareTriplets2(a2, b2)) {
			System.out.print(item2+"\t");
		}
	}
}
