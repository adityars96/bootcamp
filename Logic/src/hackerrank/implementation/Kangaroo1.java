package hackerrank.implementation;

public class Kangaroo1 {
	static String kangaroo1(int x11, int v11, int x21, int v21) {
		String result1="YES";
		if(x11<x21 && v11<v21) {
			result1="NO";
		} else {
			if(v11!=v21 && (x21-x11)%(v11-v21)==0) {
				result1="YES";
			} else {
				result1="NO";
			}
		} return result1;
	}
	
	public static void main(String[] args) {
		int x11=0; int v11=3;
		int x21=4; int v21=2;
		String hasil1=kangaroo1(x11, v11, x21, v21);
		System.out.println(hasil1);
	}
}
