package hackerrank.implementation;

public class CountingValue {
	public static void main(String[] args) {
		String s="UDDDUDUU";
		int n=8;
		String[] s1=s.split("");
		int[] h=new int[s1.length];
		for (int i = 0; i < s1.length; i++) {
			if(s1[i]=="U") {
				h[i]=1;
			} 
			else if(s1[i]=="D") {
				h[i]=-1;
			} 
			else {
				h[i]=0;
			}
		}
		
		for (int i = 0; i < s1.length; i++) {
			System.out.print(s1[i]+"=");
			System.out.print(h[i]+" ");
		}
		
	}
}
