package hackerrank.implementation;

public class DivisibleSumPairs3 {
	static int divisibleSumPairs3(int n3, int k3, int[] ar3) {
		int count3=0;
		for (int i = 0; i < n3; i++) {
			for (int j = n3-1; j > i; j--) {
				if((ar3[i]+ar3[j])%k3==0)
					count3++;
			}
		} 
		return count3;
	}
	public static void main(String[] args) {
		int n3=6; int k3=3;
		int[] ar3=new int[] {1,3,2,6,1,2};
		int hasil3=divisibleSumPairs3(n3, k3, ar3);
		System.out.print("Divisible sum pairs : "+hasil3);
	}
}
