package hackerrank.implementation;

public class AVeryBigSum2 {
	static long aVeryBigSum2(long[] ar2) {
		long sum2=0;
		for (int i = 0; i < ar2.length; i++) {
			sum2+=ar2[i];
		} return sum2;
	}
	public static void main(String[] args) {
		long[] ar2=new long[]{1000000001, 1000000002, 1000000003, 1000000004, 1000000005};
		long hasil2=aVeryBigSum2(ar2);
		System.out.print("Sum = "+hasil2);
	}
}
