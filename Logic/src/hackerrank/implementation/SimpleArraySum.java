package hackerrank.implementation;

public class SimpleArraySum {
	static int simpleArraySum(int[] ar) {
        int sum=0; //membuat nilai sum awal
        for (int i=0; i<ar.length; i++){
            sum=sum+ar[i]; //update nilai sum
        }
        return sum;

    }

    public static void main(String[] args){
    	int[] ar=new int[] {1,2,3,4,10,11}; //membuat array ar
    	int hasil=simpleArraySum(ar); //memanggil method simpleArraySum
    	System.out.print("Sum : "+hasil);
    }
}
