package hackerrank.implementation;

public class SimpleArraySum2 {
	static int simpleArraySum2(int[] ar2) {
		int sum2=0;
		for (int i = 0; i < ar2.length; i++) {
			sum2+=ar2[i];
		}
		return sum2;
	}
	
	public static void main(String[] args) {
		int[] ar2=new int[] {1,2,3,4,10,11};
		int hasil2=simpleArraySum2(ar2);
		System.out.print("Sum : "+hasil2);
	}
}
