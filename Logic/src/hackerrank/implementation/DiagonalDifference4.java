package hackerrank.implementation;

public class DiagonalDifference4 {
	static int diagonalDifference(int[][] arr4) {
		int kanan4=0; int kiri4=0;
		for (int i = 0; i < arr4.length; i++) {
			kanan4+=arr4[i][i];
			kiri4+=arr4[arr4.length-1-i][i];
		}
		int result4=Math.abs(kanan4-kiri4);
		return result4;
	}
	public static void main(String[] args) {
		int[][] arr4=new int[][] {{11,2,4},{4,5,6},{10,8,-12}};
		int hasil4=diagonalDifference(arr4);
		System.out.println("Diagonal difference : "+hasil4);
	}
	
}
