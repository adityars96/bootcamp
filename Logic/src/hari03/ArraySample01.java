package hari03;

import java.util.Scanner;
/*
public class ArraySample01 {
	static Scanner scn;
	
	public static void main(String[] args) {
		// 3 1 9 3 15 5 => n=6
		scn=new Scanner(System.in);
		System.out.print("Masukkan nilai n: ");
		int n=scn.nextInt();
		int a=3; int b=1;
		
		int[] array=new int[n];
		for (int i = 0; i < array.length; i++) {
			if(i%2==0) {
				array[i]=a;
				a+=6;
			} else {
				array[i]=b;
				b+=2;
			}
			System.out.print(array[i]+"\t");
		}
	}
}
*//*
public class ArraySample01{
	static Scanner scn;
	
	public static void main(String[] args) {
		scn=new Scanner(System.in);
		System.out.print("Masukkan nilai n: ");
		int n=scn.nextInt();
		int a=3; int b=1;
		int[] array=new int[n];
		
		for (int i = 0; i < array.length; i++) {
			if(i%2==0) {
				array[i]=a;
				a+=6;
			} else {
				array[i]=b;
				b+=2;
			}
			System.out.print(array[i]+"\t");
		}
	}
}
*/
public class ArraySample01{
	static Scanner scn;
	
	public static void main(String[] args) {
		scn=new Scanner(System.in);
		System.out.print("Masukkan nilai n: ");
		int n=scn.nextInt();
		int a=3; int b=1;
		int[] array = new int[n];
		
		for (int i = 0; i < array.length; i++) {
			if(i%2==0) {
				array[i]=a;
				a+=6;
			} else {
				array[i]=b;
				b+=2;
			}
			System.out.print(array[i]+"\t");
			
		}
	}
}