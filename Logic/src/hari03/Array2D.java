package hari03;

public class Array2D {
	public static void main(String[] args) {
		
		// mengisi
		int[][] array1=new int[5][3];
		for (int i = 0; i < array1.length; i++) {
			int a=1;
			for (int j = 0; j < array1[i].length; j++) {
				array1[i][j]=a;
				a++;
			}
		}
		
		// menampilkan
		System.out.println("Array 1");
		for (int i = 0; i < array1.length; i++) {
			// isi dari kiri ke kanan
			for (int j = 0; j < array1[i].length; j++) {
				System.out.print(array1[i][j]+"\t");
			}
			// pindah baris
			System.out.println("\n");
		}
		
		int[][] array2=new int[3][5];
		for (int i = 0; i < array2.length; i++) {
			for (int j = 0; j < array2[i].length; j++) {
				array2[i][j]=j;
			}
		}
	}
}