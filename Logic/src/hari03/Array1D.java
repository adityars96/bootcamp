package hari03;

public class Array1D {
	public static void main(String[] args) {
		// deklarasi array
		int[] array = new int[10];
		array[0]=1;
		array[1]=2;
		array[2]=3;
		array[3]=4;
		array[4]=5;
		array[5]=6;
		array[6]=7;
		array[7]=8;
		array[8]=9;
		array[9]=10;
		System.out.println("Length array pertama "+array.length);
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i]+" ");
		}
		System.out.println();
		System.out.println();
		
		// cara kedua
		int[] array2 = new int[] {1,2,3,4,5};
		System.out.println("Length array kedua "+array2.length);
		for (int i = 0; i < array2.length; i++) {
			System.out.print(array2[i]+" ");			
		}
	}
}
