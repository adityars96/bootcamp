package hari03;

import java.util.Scanner;

import common.PrintArray;

public class Soal02 {
	static Scanner scn;
	
	public static void main(String[] args) {
		scn=new Scanner(System.in);
		System.out.print("Masukkan N: ");
		int n=scn.nextInt();
		System.out.print("Masukkan M: ");
		int m=scn.nextInt();
		System.out.print("Masukkan O: ");
		int o=scn.nextInt();
		
		// Buat array deret
		int[] deret=new int[n*4];
		int x=3;
		for (int i = 0; i < deret.length; i++) {
			if (i%4==3) {
				deret[i]=x;
				x*=3;
			} else {
				deret[i]=o;
				o+=m;
			}
			System.out.print(deret[i]+" ");
		}
		System.out.println();
		System.out.println();
		
		// mengisi
		String[][] array=new String[n][n];
		int a=0;
		
		// isi diagonal kanan
		for (int i = 0; i < n; i++) {
			array[(n-1)-i][i]=""+deret[a];
			a++;
		}
		// isi kolom ke n-1
		for (int i = 1; i < n; i++) {
			array[i][n-1]=""+deret[a];
			a++;
		}
		// isi baris ke n-1
		for (int i = n-2; i>0; i--) {
			array[n-1][i]=""+deret[a];
			a++;
		}
		/*// isi kolom ke 0
		for (int i = n-2; i>0; i--) {
			array[i][0]=""+deret[a];
			a++;
		}*/
		
		// memanggil print array
		PrintArray.array2D(array);

	}
}
