package common;

public class PrintArray {
	public static void array2D(String[][] array) {
		for (int i = 0; i < array.length; i++) {
			// isi dari kiri ke kanan
			for (int j = 0; j < array[i].length; j++) {
				System.out.print(array[i][j]+"\t");
			}
			// pindah baris
			System.out.println("\n");
		}
	}
	
	public static void array2D(int[][] array) {
		for (int i = 0; i < array.length; i++) {
			// isi dari kiri ke kanan
			for (int j = 0; j < array[i].length; j++) {
				System.out.print(array[i][j]+"\t");
			}
			// pindah baris
			System.out.println("\n");
		}
	}
}
