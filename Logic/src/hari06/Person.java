package hari06;

public class Person {
	// list Property
	public int id;
	public String name;
	public String address;
	public String gender;
	// construktor Person
	public Person() {
		System.out.println("Memanggil Constructor");
	}
	// construktor Person
	public Person(int id, String name, String address, String gender) {
		this.id=id;
		this.name=name;
		this.address=address;
		this.gender=gender;
	}
	// display
	public void display() {
		System.out.println("ID \t: "+id);
		System.out.println("Name \t: "+name);
		System.out.println("Address \t: "+address);
		System.out.println("Gender \t: "+gender);
	}
}
