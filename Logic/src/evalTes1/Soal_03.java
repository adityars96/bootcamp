package evalTes1;

public class Soal_03 {
	static void methodSoal03(int n, int[] deret) {
		int[] deret1=new int[deret.length];
		for (int i = 0; i < deret.length; i++) {
			if(deret[i]%3==0) {
				deret[i]=deret[i]/3;
			}
		}
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < deret.length; j++) {
				if(j==0) {
					deret1[deret.length-1]=deret[0];
				} else {
					deret1[j-1]=deret[j];
				}
			}
			for (int k = 0; k < deret1.length; k++) {
				deret[k]=deret1[k];
			}
		}
		for (int l = 0; l < deret.length; l++) {
			System.out.print(deret[l]+" ");
		};
	}
	public static void main(String[] args) {
		int[] deret=new int[] {7,3,9,9,2,12,15};
		methodSoal03(7, deret);
	}
}
