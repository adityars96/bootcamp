package evalTes1;

public class Soal_04 {
	static void methodSoal04(float[] bill) {
		int[] bill5=new int[bill.length];
		for (int i = 0; i < bill.length; i++) {
			bill[i]=bill[i]*115/100;
		}
		float bayar=0;
		float bayar5=0;
		for (int j = 1; j < bill.length; j++) {
			bayar+=bill[j];
			bayar5+=bill5[j];
		}
		bayar=bayar/4;
		bayar5=bayar5/4;
		float bayar1=bayar+bill[0]/3;
		float bayar6=bayar5+bill5[0]/3;
		System.out.println("Bill teman 1 : "+bayar);
		System.out.println("Bill lainnya : "+bayar1);
		System.out.println();
		System.out.println("Bill teman 1 : "+bayar5);
		System.out.println("Bill lainnya : "+bayar6);
	}
	public static void main(String[] args) {
		float[] bill=new float[] {33000,45000,15000,80000};
		methodSoal04(bill);
	}
}
