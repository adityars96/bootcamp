package evalTes1;

public class Soal_09 {
	static void methodSoal09(int jam, int menit) {
		int sudut=0;
		if(jam*60+menit>1439) {
			System.out.println("Waktu tidak valid");
		}else {
			if(jam>11) {
				jam=jam-12;
			}
			sudut=jam*30+menit/2-menit*6;
			System.out.println(Math.abs(sudut));
		}
	}
	public static void main(String[] args) {
		methodSoal09(3, 00);
		methodSoal09(5, 30);
		methodSoal09(2, 20);
		methodSoal09(3, 15);
		methodSoal09(23, 59);
	}
}
