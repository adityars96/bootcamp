package evalTes1;

import java.util.Arrays;

public class Soal_07 {
	static void methodSoal07(int[] deret) {
		float mean=0;
		int median=0;
		int modus=0;
		int count=0;
		for (int i = 0; i < deret.length; i++) {
			mean+=deret[i];
		}
		mean=mean/deret.length;
		System.out.println("Mean : "+mean);
		
		Arrays.sort(deret);
		if(deret.length%2==0) {
			median=deret[(deret.length/2)-1]+deret[(deret.length/2)];
			median=median/2;
		} else {
			median=deret[(deret.length-1)/2];
		}
		System.out.println("Median : "+median);
		
	}
	
	public static void main(String[] args) {
		int[] deret=new int[] {1,1,3,4,5,6};
		methodSoal07(deret);
	}
}
