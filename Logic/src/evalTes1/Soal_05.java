package evalTes1;

public class Soal_05 {
	static void methodSoal05(int jam, int menit, int detik) {
		if(jam*3600+menit*60+detik>86400) {
			System.out.println("Waktu tidak valid");
		} else {
			if(jam*3600+menit*60+detik<43200) {
				if(jam==00) {
					jam=12;
				}
				System.out.println(jam+":"+menit+":"+detik+" AM");
			} else {
				jam=jam-12;
				if(jam==0) {
					jam=12;
				}
				System.out.println(jam+":"+menit+":"+detik+" PM");
			}
		}
	}
	public static void main(String[] args) {
		methodSoal05(25, 40, 44);
	}
}
