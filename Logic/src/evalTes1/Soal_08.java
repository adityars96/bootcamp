package evalTes1;

import java.util.Arrays;

public class Soal_08 {
	static void methodSoal08(int[] deret) {
		Arrays.sort(deret);
		int minsum=0; int maxsum=0;
		for (int i = 0; i < deret.length; i++) {
			if(i<4) {
				minsum+=deret[i];
			}
			if(i>(deret.length-1)-4) {
				maxsum+=deret[i];
			}
		}
		System.out.println("Max sum : "+maxsum);
		System.out.println("Min sum : "+minsum);
	}
	
	public static void main(String[] args) {
		int[] deret=new int[] {0,0,-1,0,1,1};
		methodSoal08(deret);
	}
}
