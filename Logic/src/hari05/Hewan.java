package hari05;

public class Hewan {
	String spesies;
	String jenis;
	String habitat;
	String kembangbiak;
	
	public Hewan(String spesies, String jenis, String habitat, String kembangbiak) {
		this.spesies=spesies;
		this.jenis=jenis;
		this.habitat=habitat;
		this.kembangbiak=kembangbiak;
	}
	
	public Hewan(String spesies, String jenis) {
		this.spesies=spesies;
		this.jenis=jenis;
	}
	public void showData(){
		System.out.println("Spesies \t : "+this.spesies);
		System.out.println("Jenis \t \t : "+this.jenis);
		System.out.println("Habitat \t : "+this.habitat);
		System.out.println("Berkembangbiak \t : "+this.kembangbiak);
		System.out.println();
	}
}
