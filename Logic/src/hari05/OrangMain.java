package hari05;

public class OrangMain {
	public static void main(String[] args) {
		Orang  org1=new Orang(1, "Aditya Rizki Saputro", "Jakarta", "Pria", 22);
		System.out.println("Data orang 1: ");
		org1.showData();
		
		Orang org2=new Orang(2, "Shella", "Depok");
		System.out.println("Data orang 2: ");
		org2.showData();
		
		Orang org3=new Orang();
		System.out.println("Data orang 3: ");
		org3.showData();
		
		Orang org4=org2;
		System.out.println("Data orang 4: ");
		org4.nama="Agus";
		org4.showData();
		
		System.out.println("Data orang 2: ");
		org2.showData();
	}
}
