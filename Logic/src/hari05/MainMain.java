package hari05;

public class MainMain {
	public static void main(String[] args) {
		Orang  org1=new Orang(1, "Aditya Rizki Saputro", "Jakarta", "Pria", 22);
		System.out.println("Data orang 1: ");
		org1.showData();
		
		Alamat alm1=new Alamat("Jl Basmol", "Kembangan Utara", "Kembangan", "Jakarta", "Indonesia");
		System.out.println("Data alamat 1: ");
		alm1.showData();
		
		Hewan hwn1=new Hewan("Kucing", "Mamalia", "Darat", "Melahirkan");
		System.out.println("Data hewan 1: ");
		hwn1.showData();
		
		Kendaraan kdr1=new Kendaraan("B 5437 BIB", "Motor", "Honda", "Hitam", 2015, 100);
		System.out.println("Data kendaraan 1: ");
		kdr1.showData();
		
		Mahasiswa mhs1=new Mahasiswa("Aditya Rizki Saputro", "Universitas Indonesia", "Matematika", 7);
		System.out.println("Data mahasiswa 1: ");
		mhs1.showData();
		
		Planet pln1=new Planet("Bumi", "Batuan", 3, true);
		System.out.println("Data planet 1: ");
		pln1.showData();
	}
}