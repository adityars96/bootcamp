package hari05;

public class Mahasiswa {
	String nama;
	String univ;
	String prodi;
	int semester;
	
	public Mahasiswa(String nama, String univ, String prodi, int semester) {
		this.nama=nama;
		this.univ=univ;
		this.prodi=prodi;
		this.semester=semester;
	}
	
	public void showData(){
		System.out.println("Nama \t : "+this.nama);
		System.out.println("Univ \t : "+this.univ);
		System.out.println("Prodi \t : "+this.prodi);
		System.out.println("Semester : "+this.semester);
		System.out.println();
	}
}
