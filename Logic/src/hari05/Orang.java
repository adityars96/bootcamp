package hari05;

public class Orang {
	// properti
	int id;
	String nama;
	String alamat;
	String jk;
	int umur;
	
	// construktor
	public Orang() {
	}
	// construktor
	public Orang(int id, String nama, String alamat, String jk, int umur) {
		this.id=id;
		this.nama=nama;
		this.alamat=alamat;
		this.jk=jk;
		this.umur=umur;
	}
	// construktor
	public Orang(int id, String nama, String alamat) {
		this.id=id;
		this.nama=nama;
		this.alamat=alamat;
	}
	// method
	public void showData() {
		System.out.println("ID \t : "+this.id);
		System.out.println("Nama \t : "+this.nama);
		System.out.println("Alamat \t : "+this.alamat);
		System.out.println("JK \t : "+this.jk);
		System.out.println("Umur \t : "+this.umur);
		System.out.println();
	}
}
