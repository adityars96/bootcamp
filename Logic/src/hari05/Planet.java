package hari05;

public class Planet {
	String nama;
	String tipe;
	int urutan;
	boolean penghuni;
	
	public Planet(String nama, String tipe, int urutan, boolean penghuni) {
		this.nama=nama;
		this.tipe=tipe;
		this.urutan=urutan;
		this.penghuni=penghuni;
	}
	public void showData() {
		System.out.println("Nama \t : "+this.nama);
		System.out.println("Tipe \t : "+this.tipe);
		System.out.println("Urutan \t : "+this.urutan);
		System.out.println("Penghuni : "+this.penghuni);
		System.out.println();
	}
}
