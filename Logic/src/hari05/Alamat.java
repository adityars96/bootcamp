package hari05;

public class Alamat {
	String jalan;
	String kel;
	String kec;
	String prov;
	String negara;
	
	public Alamat(String jalan, String kel, String kec, String prov, String negara) {
		this.jalan=jalan;
		this.kel=kel;
		this.kec=kec;
		this.prov=prov;
		this.negara=negara;
	}
	public void showData() {
		System.out.println("Jalan \t \t : "+this.jalan);
		System.out.println("Kelurahan \t : "+this.kel);
		System.out.println("Kecamatan \t : "+this.kec);
		System.out.println("Provinsi \t : "+this.prov);
		System.out.println("Negara \t \t : "+this.negara);
		System.out.println();
	}
}
