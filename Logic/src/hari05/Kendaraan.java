package hari05;

public class Kendaraan {
	String plat;
	String tipe;
	String merk;
	String warna;
	int tahun;
	int cc;
	
	public Kendaraan(String plat, String tipe, String merk, String warna, int tahun, int cc) {
		this.plat=plat;
		this.tipe=tipe;
		this.merk=merk;
		this.warna=warna;
		this.tahun=tahun;
		this.cc=cc;
	}
	public void showData(){
		System.out.println("Plat \t : "+this.plat);
		System.out.println("Tipe \t : "+this.tipe);
		System.out.println("Merk \t : "+this.merk);
		System.out.println("Warna \t : "+this.warna);
		System.out.println("Tahun \t : "+this.tahun);
		System.out.println("CC \t : "+this.cc);
		System.out.println();
	}
}
