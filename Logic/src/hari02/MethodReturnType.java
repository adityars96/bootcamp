package hari02;

import java.util.Scanner;

public class MethodReturnType {
	static Scanner scn;
	
	public static void main(String[] args) {
		scn=new Scanner(System.in);
		System.out.print("Masukkan Nilai a : ");
		int a=scn.nextInt();
		System.out.print("Masukkan Nilai b : ");
		int b=scn.nextInt();
		int c=minFunction(a,b);
		System.out.println("Minimum Value = "+c);
	}

	private static int minFunction(int n1, int n2) {
		int min;
		if(n1>n2)
			min=n2;
		else
			min=n1;
		return min;
	}
}
