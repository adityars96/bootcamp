package hari02;

import java.util.Scanner;

public class MethodNonReturnType {
	static Scanner scn;

	public static void main(String[] args) {
		scn=new Scanner(System.in);
		System.out.print("Masukkan Point : ");
		double a=scn.nextDouble();
		methodRankPoints(a);
	}

	public static void methodRankPoints(double points) {
		if (points >= 202.5) {
			System.out.println("Rank A1");
		} else if (points >= 122.4) {
			System.out.println("Rank A2");
		} else {
			System.out.println("Rank A3");
		}
		
	}
}
