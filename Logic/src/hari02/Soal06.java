package hari02;

import java.util.Scanner;

public class Soal06 {
	static Scanner scn;
	
	public static void main(String[] args) {
		scn = new Scanner(System.in);
		System.out.println("Masukkan Input : ");
		String s=scn.next(); //input
		int u=0;
		
		for (int i = 0; i < s.length(); i++) {
			if(Character.isUpperCase(s.charAt(i))) { //mencari uppercase
				u++; //menghitung uppercase
			}
		}
		System.out.println("Output: "+u);
	}	
}
