package hari02;

import java.util.Scanner;

public class Soal04 {
	static Scanner scn;
	
	public static void main(String[] args) {
		//1 5 2 10 3 15 4
		scn=new Scanner(System.in);
		System.out.print("Masukkan N1 : ");
		int n1=scn.nextInt();
		System.out.print("Masukkan N2 : ");
		int n2=scn.nextInt();
		int a=1;
		int b=a;
		int hitung=0;
		
		for (int i = 0; i < n1; i++) {
			System.out.print(i);
			System.out.print(" ");
		}
		
		System.out.println("");
		
		for (int i = 0; i < n1; i++) {
			if (i%2==0) {
				System.out.print(a+" ");
				a+=1;
			} else {
				b=(a-1)*n2;
				System.out.print(b+" ");
				hitung+=b;
			}
		}
		System.out.println("");
		
		System.out.print("Perhitungan: "+hitung);
	}
}
