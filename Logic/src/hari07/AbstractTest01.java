package hari07;

public class AbstractTest01 {

	public static void main(String[] args) {
		Bike01 obj01=new Honda01();
		Bike01 obj02=new Yamaha01();
		Bike01 obj03=new Suzuki01();
		obj01.run(); obj01.fuel();obj01.speed();
		obj02.run();
		obj03.run();
		System.out.println();
		Shape01 c1=new Circle01();
		Shape01 r1=new Rectangle01();
		c1.draw(); r1.draw();
		System.out.println();
		Univ01 un1=new UI01();
		Univ01 un2=new UNJ01();
		Univ01 un3=new UIN01();
		Univ01 un4=new ITB01();
		Univ01 un5=new IPB01();
		un1.run();un2.run();un3.run();un4.run();un5.run();
	}

}
